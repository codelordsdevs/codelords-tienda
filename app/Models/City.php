<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model {
    protected $table = 'city';
    use HasFactory;

    protected $fileable = [
        'id',
        'name',
        'region_id',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public function region() {
        return $this->belongsTo('App\Models\Region');
    }
}
