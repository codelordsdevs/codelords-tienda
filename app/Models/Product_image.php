<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_image extends Model
{
    protected $table = 'product_image';
    use HasFactory;
    use SoftDeletes;

    protected $fileable = [
        'url',
        'order',
        'featured',
        'product_id',
        'created_at',
        'updated_at'
    ];
}
