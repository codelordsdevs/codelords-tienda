<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supply extends Model
{
    protected $table = 'supply';
    use HasFactory;

    protected $fileable = [
        'id',
        'product_id',
        'qty',
        'deleted_at',
        'created_at',
    ];

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }
}
