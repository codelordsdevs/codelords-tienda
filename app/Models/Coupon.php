<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model {
    protected $table = 'coupon';
    use HasFactory;
    public $timestamps = false;
    protected $fileable = [
        'id',
        'code',
        'user_id',
        'product_id',
        'category_id',
        'qty',
        'created_at',
        'expired_at'
    ];

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }
    
    public function category(){
    	return $this->belongsTo('App\Models\Category');
    }
    
    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}
