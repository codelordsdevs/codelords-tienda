<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DispatchAdress extends Model
{
    protected $table = 'dispatch_adress';
    use HasFactory;

    protected $fileable = [
        'id',
        'postal_code',
        'name',
        'address',
        'city_id',
        'user_id',
        'receiver_name',
        'phone', 
        'email',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
}
