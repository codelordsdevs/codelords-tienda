<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'region';
    use HasFactory;

    protected $fileable = [
        'id',
        'name',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public function cities() {
        return $this->hasMany('App\Models\City');
    }
}
