<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TagProduct extends Model {
    protected $table = 'tag_product';
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'id',
        'product_id',
        'tag_id',
    ];

    public function tag() {
        return $this->belongsTo('App\Models\Tag');
    }
    public function product() {
        return $this->belongsTo('App\Models\Product');
    }
}
