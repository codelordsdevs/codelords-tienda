<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'config';
    use HasFactory;
	 public $timestamps = false;
    protected $fileable = [
        'name',
        'primary_color',
        'secondary_color',
        'url_logo',
        'url_facebook',
        'url_instagram',
        'url_twitter',
        'url_linkedin',
        'email_contact',
        'phone_number',
        'whatsapp_number',
        'address',
        'deposit_name',
        'deposit_rut',
        'deposit_email',
        'deposit_number',
        'deposit_bank',
        'deposit_account_type'
    ];
}
