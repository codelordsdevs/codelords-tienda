<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $table = 'product';
    use HasFactory;
    use SoftDeletes;

    protected $fileable = [
        'name',
        'price',
        'stock',
        'special_price',
        'created_at',
        'updated_at',
        'category_id',
        'featured',
        'description',
        'special_price_end_date'
    ];

    public function product_images(){
        return $this->hasMany('App\Models\Product_image');
    }

    public function category(){
    	return $this->belongsTo('App\Models\Category');
    }

    public function subproducts(){
    	return $this->hasMany('App\Models\Product');
    }

    public function tags(){
    	return $this->hasMany('App\Models\TagProduct');
    }
}
