<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleState extends Model
{
    protected $table = 'sale_state';
    use HasFactory;

    protected $fileable = [
        'id',
        'name',
        'deleted_at',
        'created_at',
        'updated_at',
    ];
}
