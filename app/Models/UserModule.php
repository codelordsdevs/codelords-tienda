<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserModule extends Model {
    protected $table = 'user_module';
    use HasFactory;

    public function module() {
        return $this->belongsTo('App\Models\Module');
    }

    public function user() {
        return  $this->belongsTo('App\Models\User');
    }
}
