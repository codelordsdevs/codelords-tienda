<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model {
    protected $table = 'sale_detail';
    use HasFactory;

    protected $fileable = [
        'id',
        'sale_id',
        'product_id',
        'qty',
        'unit_price',
        'total_price',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }
}
