<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    use HasFactory;

    protected $fileable = [
        'name',
        'category_id',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

	public function subcategories(){
		return $this->hasMany('App\Models\Category');
	}

    	public function products(){
		return $this->hasMany('App\Models\Product');
	}
}
