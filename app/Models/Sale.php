<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model {
    protected $table = 'sale';
    use HasFactory;

    protected $fileable = [
        'id',
        'user_id',
        'payment_id',
        'sale_state_id',
        'address',
        'city_id',
        'deleted_at',
        'created_at',
        'updated_at',
        'receiver_name',
        'phone',
        'email',
        'postal_code',
        'pickup',
    ];


    public function sale_detail() {
        return $this->hasMany('App\Models\SaleDetail');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function city() {
        return $this->belongsTo('App\Models\City');
    }
    
    public function sale_state() {
        return $this->belongsTo('App\Models\SaleState');
    }
}
