<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SpecialPrice;
use App\Models\Product;
use App\Models\User;
use App\Models\Product_has_user;

class SpecialPriceController extends Controller
{
    //
    public function get(Request $request)
    {
        //$specialPrice = SpecialPrice::where('product_id', request('idUser'))->where('user_id', request('idUser'));
        $specialPrice = SpecialPrice::get();
        if ($specialPrice != null) {
            //tenimo precio especial x cliente
            $specialPrice = $specialPrice->special_price;
        } else {
            //no tenimo asik bucamo el del producto solito
            $product = Product::where('id', request('product_id'))->find(request('product_id'));
            if ($product->special_price != null) {
                //tenimo precio especial x cliente
                $specialPrice = $product->special_price;
            } else {
                $specialPrice = $product->price;
            }
        }
        return $specialPrice;
    }

    public function post(Request $request)
    {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        if (request('idProducts') != null && request('idUser') != null) {
            $new_special_price = new specialPrice();
            $new_special_price->product_id = request('idProducts');
            $new_special_price->user_id = request('idUser');
            $new_special_price->special_price = request('specialPrice');
            $new_special_price->save();
            return [
                'status' => 'true',
                'message' => 'Insertado',
            ];
        }
    }

    public function delete(Request $request)
    {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        if (request('product_id') != null && request('user_id') != null) {
            $product_has_user = Product_has_user::where('user_id', request('user_id'))->where('product_id', request('product_id'))->first();
            $product_has_user->delete();
            return [
                'status' => 'true',
                'message' => 'Eliminado',
            ];
        }
    }
}
