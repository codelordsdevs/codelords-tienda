<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Tag;
use App\Models\TagProduct;
use App\Http\Resources\TagResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller {
    public function __construct() {
        $this->middleware('auth:api', ['except' => []]);
    }

    public function post(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador
        if (request('product_id') != null && request('product_id') > 0) {
            return TagResource::collection(Product::with('tags.tag')->find(request('product_id'))->tags);
        }
    }

    public function put(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        $rules = [
            'product_id' => 'required|integer|exists:product,id',
            'tag_name' => 'required|string',
        ];
        $customMessages = [
            'required' => ':attribute campo requerido.',
            'max' => ':attribute supera cantidad maxima de caracteres.',
            'unique' => ':attribute ya existe.',
            'integer' => ':attribute debe ser numerico.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            //validar el stock de los productos antes de esto
            try {
                $tag = Tag::firstOrCreate(['name' => request('tag_name')]);
                $tag_product = TagProduct::firstOrCreate(['tag_id' => $tag->id], ['product_id' => request('product_id')]);
                
                return [
                    'status' => 'true',
                    'message' => 'Stock cargado al producto',
                    'tags' => TagResource::collection(Product::with('tags.tag')->find(request('product_id'))->tags),
                ];

            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Error interno',
                ];
            }
        }
    }

    public function delete(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        if (request('id') != null && request('id') > 0) {
            $tag = TagProduct::find(request('id'));
            try {
                $tag->delete();
                return [
                    'status' => 'true',
                    'message' => 'tag eliminado',
                ];
            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Error interno',
                ];
            }
        }
        return [
            'status' => 'false',
            'message' => 'Request sin ID',
        ];
    }
}
