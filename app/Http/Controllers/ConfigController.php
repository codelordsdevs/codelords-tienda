<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Config;
use App\Http\Resources\ConfigResource;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\File;

class ConfigController extends Controller {
	public function __construct() {
		$this->middleware('auth:api', ['except' => ['get']]);
	}

	public function get() {
		return ConfigResource::collection(Config::all());
	}

	public function post(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador
		if (request('image_upload') != null && request('image_upload') == 1) {
			$rules = [
				'file' => 'required|image',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$image_path = "/images/logo_web.png";  // Value is not URL but directory file path
					if (File::exists($image_path)) {
						File::delete($image_path);
					}
					$fileName = 'logo_web.png';
					$request->file->move(public_path('image'), $fileName);
					$webconfiguration = Config::find(1);
					$webconfiguration->url_logo = env('APP_URL') . '/image/logo_web.png';
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Nueva imagen logo insertado',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if (request('image_carousel1') != null && request('image_carousel1') == 1) {
			$rules = [
				'file' => 'required|image',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$image_path = "/images/carousel1.png";  // Value is not URL but directory file path
					if (File::exists($image_path)) {
						File::delete($image_path);
					}
					$fileName = 'carousel1.png';
					$request->file->move(public_path('image'), $fileName);
					$webconfiguration = Config::find(1);
					$webconfiguration->url_image_carousel1 = env('APP_URL') . '/image/carousel1.png';
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Nueva imagen carousel1 insertado',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if (request('image_carousel2') != null && request('image_carousel2') == 1) {
			$rules = [
				'file' => 'required|image',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$image_path = "/images/carousel2.png";  // Value is not URL but directory file path
					if (File::exists($image_path)) {
						File::delete($image_path);
					}
					$fileName = 'carousel2.png';
					$request->file->move(public_path('image'), $fileName);
					$webconfiguration = Config::find(1);
					$webconfiguration->url_image_carousel2 = env('APP_URL') . '/image/carousel2.png';
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Nueva imagen carousel2 insertado',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if (request('image_parallax') != null && request('image_parallax') == 1) {
			$rules = [
				'file' => 'required|image',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$image_path = "/images/parallax.png";  // Value is not URL but directory file path
					if (File::exists($image_path)) {
						File::delete($image_path);
					}
					$fileName = 'parallax.png';
					$request->file->move(public_path('image'), $fileName);
					$webconfiguration = Config::find(1);
					$webconfiguration->url_image_parallax = env('APP_URL') . '/image/parallax.png';
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Nueva imagen parallax insertado',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if (request('image_products') != null && request('image_products') == 1) {
			$rules = [
				'file' => 'required|image',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$image_path = "/images/products.png";  // Value is not URL but directory file path
					if (File::exists($image_path)) {
						File::delete($image_path);
					}
					$fileName = 'products.png';
					$request->file->move(public_path('image'), $fileName);
					$webconfiguration = Config::find(1);
					$webconfiguration->url_image_products = env('APP_URL') . '/image/products.png';
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Nueva imagen productos insertado',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if (request('set_name') != null && request('set_name') == 1) {
			$rules = [
				'name' => 'required|string',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$webconfiguration = Config::find(1);
					$webconfiguration->name = request('name');
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Nombre de la tienda actualizado correctamente',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if (request('set_email') != null && request('set_email') == 1) {
			$rules = [
				'email' => 'required|string',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$webconfiguration = Config::find(1);
					$webconfiguration->email_contact = request('email');
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Email de la tienda actualizado correctamente',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if (request('set_phone') != null && request('set_phone') == 1) {
			$rules = [
				'phone' => 'required|string',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$webconfiguration = Config::find(1);
					$webconfiguration->phone_number = request('phone');
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Telefono de la tienda actualizado correctamente',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if (request('set_whatsapp') != null && request('set_whatsapp') == 1) {
			$rules = [
				'whatsapp' => 'required|string',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$webconfiguration = Config::find(1);
					$webconfiguration->whatsapp_number = request('whatsapp');
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Whatsapp de la tienda actualizado correctamente',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if (request('set_adress') != null && request('set_adress') == 1) {
			$rules = [
				'adress' => 'required|string',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$webconfiguration = Config::find(1);
					$webconfiguration->address = request('adress');
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Direccion de la tienda actualizado correctamente',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if (request('set_primary_color') != null && request('set_primary_color') == 1) {
			$rules = [
				'primary_color' => 'required|string',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$webconfiguration = Config::find(1);
					$webconfiguration->primary_color = request('primary_color');
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Color Primario de la tienda actualizado correctamente',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if (request('set_secondary_color') != null && request('set_secondary_color') == 1) {
			$rules = [
				'secondary_color' => 'required|string',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$webconfiguration = Config::find(1);
					$webconfiguration->secondary_color = request('secondary_color');
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Color Secundario de la tienda actualizado correctamente',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}

		if (request('set_social_network') != null && request('set_social_network') == 1) {
			$rules = [
				'url' => 'required|string',
				'set_url' => 'required|string',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$webconfiguration = Config::find(1);
					switch (request('set_url')):
						case 'facebook':
							$webconfiguration->url_facebook = request('url');
							break;
						case 'instagram':
							$webconfiguration->url_instagram = request('url');
							break;
						case 'twitter':
							$webconfiguration->url_twitter = request('url');
							break;
						case 'linkedin':
							$webconfiguration->url_linkedin = request('url');
							break;
					endswitch;
					$webconfiguration->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Red social de la tienda actualizado correctamente',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
	}
}
