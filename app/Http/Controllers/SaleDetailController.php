<?php

namespace App\Http\Controllers;

use App\Http\Resources\SaleDetailResource;
use App\Models\SaleDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class SaleDetailController extends Controller
{
	public function __construct(){
        	$this->middleware('auth:api');
	}

    public function post(Request $request){
        if(request('sale_id') != null && request('sale_id') > 0){
            return SaleDetailResource::collection(SaleDetail::where('sale_id',request('sale_id'))->get());
        }
    }
    public function put(Request $request)
    {
        $rules = [
            'sale_id' => [
                'required',
                'integer',
            ],
            'unit_price' => [
                'required',
                'integer',
            ],
            'product_id' => [
                'required',
                'integer',
            ],
            'qty' => [
                'required',
                'integer',
            ],
            'total_price' => [
                'required',
                'integer',
            ]
        ];
        $customMessages = [
            'required' => ':attribute campo requerido.',
            'max' => ':attribute supera cantidad maxima de caracteres.',
            'unique' => ':attribute ya existe.',
            'integer' => ':attribute debe ser numerico.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            //If you don't have an id in the request, a new one is added
            try {
                $SaleDetailExists = SaleDetail::where('id', request('id'))->where('sale_id', request('sale_id'))->first();
		if ($SaleDetailExists) {

            $SaleDetailExists->sale_id = request('sale_id');
            $SaleDetailExists->product_id = request('product_id');
            $SaleDetailExists->qty = request('qty');
            $SaleDetailExists->total_price = request('total_price');
            $SaleDetailExists->unit_price = request('unit_price');
            $SaleDetailExists->save();

        }else{

                	$SaleDetail = new SaleDetail();
                    $SaleDetail->sale_id = request('sale_id');
                    $SaleDetail->product_id = request('product_id');
                    $SaleDetail->qty = request('qty');
                    $SaleDetail->total_price = request('total_price');
                    $SaleDetail->unit_price = request('unit_price');
                    $SaleDetail->save();
                    	return [
                        	'status' => 'true',
                        	'message' => 'Sale Detail added successfully',
                    	];
                }
            }catch(Exception $ex){
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Internal Error',
                ];
            }
        }
    }

    public function delete(Request $request){
        if(request('id') != null && request('id') > 0) {
            $SaleDetail = SaleDetail::where('id', request('id'))->get()->first();
            if (($SaleDetail  != null)) {
                try {
                    $SaleDetail->delete();
                    return [
                        'status' => 'true',
                        'message' => 'Shipping address removed',
                    ];
                }catch(Exception $ex){
                    //poner esto en un log
                    return [
                        'status' => 'false',
                        'message' => 'Internal Error',
                    ];
                }
            }
        }
        return [
            'status' => 'false',
            'message' => 'id not found',
        ];
    }
}
