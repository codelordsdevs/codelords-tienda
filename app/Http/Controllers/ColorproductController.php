<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Validator;

class ColorproductController extends Controller {
	public function __construct() {
		$this->middleware('auth:api');
	}

	public function get() {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		return ProductResource::collection(Product::where('product_id', request('id'))->where('product_type_id', 2)->with('product_images')->get());
	}

	public function put(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		$rules = [
			'name' => 'required|max:100|unique:product,name',
			'price' => 'required|integer',
			'stock' => 'required|integer',
			'share_stock' => 'required|max:4',
			'color_code' => 'required|max:100',
			'special_price' => 'integer',
			'product_id' => 'required|integer|exists:product,id'
		];
		$customMessages = [
			'required' => ':attribute campo requerido.',
			'max' => ':attribute supera cantidad maxima de caracteres.',
			'unique' => ':attribute ya existe.',
			'integer' => ':attribute debe ser numerico.'
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return $validator->messages();
		} else {
			$father_product = Product::find(request('product_id'));
			$product = new Product();
			try {
				$product->name = request('name');
				$product->price = request('price');
				$product->stock = request('stock');
				$product->special_price = request('special_price');
				$product->category_id = $father_product->category_id;
				$product->featured = 0;
				$product->description = 'NA';
				$product->special_price_end_date = request('special_price_end_date');
				$product->product_id = request('product_id');
				$product->color_code = request('color_code');
				$product->product_type_id = 2;
				$product->share_stock = request('share_stock');
				$product->save();
				return [
					'status' => 'true',
					'message' => 'Producto creado',
					'products' => ProductResource::collection(Product::where('product_id', request('product_id'))->where('product_type_id', 2)->with('product_images')->get())
				];
			} catch (Exception $ex) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}

	public function patch(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		if (request('id') != null && request('id') > 0) {
			$product = Product::where('id', request('id'))->get();
			if (($product[0] != null)) {
				$rules = [
					'name' => 'required|max:100|unique:product,name',
					'price' => 'required|integer',
					'special_price' => 'integer',
					'share_stock' => 'required|max:4',
					'color_code' => 'required|max:100',
				];
				$customMessages = [
					'required' => ':attribute campo requerido.',
					'max' => ':attribute supera cantidad maxima de caracteres.',
					'unique' => ':attribute ya existe.',
					'integer' => ':attribute debe ser numerico.'
				];
				$validator = Validator::make($request->all(), $rules, $customMessages);
				if ($validator->fails()) {
					return $validator->messages();
				} else {
					try {
						if ((request('name') != null) && (strlen(request('name')) > 0)) {
							$product[0]->name = request('name');
						}
						$product[0]->price = request('price');
						$product[0]->special_price = request('special_price');
						$product[0]->share_stock = request('share_stock');
						$product[0]->color_code = request('color_code');
						$product[0]->special_price_end_date = request('special_price_end_date');
						$product[0]->save();
						return [
							'status' => 'true',
							'message' => 'Producto actualizado',
							'products' => ProductResource::collection(Product::where('product_id', $product[0]->product_id)->where('product_type_id', 2)->with('product_images')->get())
						];
					} catch (Exception $ex) {
						//poner esto en un log
						return [
							'status' => 'false',
							'message' => 'Error interno',
						];
					}
				}
			} else {
				return [
					'status' => 'false',
					'message' => 'Producto no encontrado',
				];
			}
		}
		return [
			'status' => 'false',
			'message' => 'Request sin ID',
		];
	}

	public function delete(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		if (request('id') != null && request('id') > 0) {
			$product = Product::find(request('id'));
			if (($product != null) && ($product->stock == 0)) {
				try {
					$product->delete();
					return [
						'status' => 'true',
						'message' => 'Producto eliminado',
						'products' => ProductResource::collection(Product::with('product_images')->get())

					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			} else {
				return [
					'status' => 'false',
					'message' => 'Producto no se puede eliminar, tiene stock mayor a 0',
				];
			}
		}
		return [
			'status' => 'false',
			'message' => 'Request sin ID',
		];
	}

	public function options(Request $request) {
	}
}
