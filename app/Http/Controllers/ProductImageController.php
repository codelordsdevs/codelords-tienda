<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product_image;
use App\Http\Resources\ProductImageResource;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\File;


class ProductImageController extends Controller {
	public function __construct() {
		$this->middleware('auth:api');
	}

	public function post(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		$rules = [
			'product_id' => 'required|integer|exists:product,id',
		];
		$customMessages = [
			'required' => ':attribute campo requerido.',
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return $validator->messages();
		} else {

			return ProductImageResource::collection(Product_image::where('product_id', '=', request('product_id'))->orderBy('order', 'ASC')->get());
		}
	}

	public function put(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		$rules = [
			'product_id' => 'required|integer|exists:product,id',
			'file' => 'required|image',
		];
		$customMessages = [
			'required' => ':attribute campo requerido.',
			'unique' => ':attribute ya existe.',
			'string' => ':attribute debe ser texto.',
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return $validator->messages();
		} else {
			try {
				$fileName = time() . '_' . request('product_id') . '.' . $request->file->extension();

				$request->file->move(public_path('image/products/product_' . request('product_id')), $fileName);

				$image = new Product_image();
				$image->product_id = request('product_id');
				$image->url = env('APP_URL') . '/image/products/product_' . request('product_id') . '/' . $fileName;
				$image->order = 1;
				$image->featured = 1;
				$image->save();

				return [
					'status' => 'true',
					'message' => 'Nueva imagen insertada',
					'images' => ProductImageResource::collection(Product_image::where('product_id', '=', request('product_id'))->orderBy('order', 'ASC')->get()),
				];
			} catch (Exception $ex) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}

	public function delete(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		$rules = [
			'id' => 'required|integer|exists:product_image,id',
			'product_id' => 'required|integer|exists:product,id',

		];

		$customMessages = [
			'required' => ':attribute campo requerido.',
			'exists' => ':attribute ya existe.',
			'integer' => ':attribute debe ser texto.',
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return $validator->messages();
		} else {
			try {
				$filename = str_replace(env('APP_URL'), "", Product_image::where('id', request('id'))->first()->url);
				if (File::exists($filename)) {
					File::delete($filename);
				}
				$product_image = Product_image::where('id', request('id'))->delete();
				return [
					'status' => 'true',
					'message' => 'imagen eliminada',
					'images' => ProductImageResource::collection(Product_image::where('product_id', '=', request('product_id'))->orderBy('order', 'ASC')->get()),
				];
			} catch (Exception $ex) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}

	public function patch(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		$rules = [
			'order' => 'required|integer',
			'image_id' => 'required|integer|exists:product_image,id',

		];

		$customMessages = [
			'required' => ':attribute campo requerido.',
			'exists' => ':attribute ya existe.',
			'integer' => ':attribute debe ser texto.',
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return $validator->messages();
		} else {
			try {
				$product_image = Product_image::where('id', request('image_id'))->first();
				$product_image->order = request('order');
				$product_image->save();
				return [
					'status' => 'true',
					'message' => 'imagen actualizada',
					'images' => ProductImageResource::collection(Product_image::where('product_id', '=', $product_image->product_id)->orderBy('order', 'ASC')->get()),
				];
			} catch (Exception $ex) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}
}
