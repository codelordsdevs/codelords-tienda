<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Models\ContactUs;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //    public function __construct()
    //    {
    //        $this->middleware('auth');
    //    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        echo '<a href="https://wwww.codelords.cl">Be a lord</a>';
    }

    public function contactPost(Request $request) {
        $rules = [
            'name' => 'required|max:50',
            'email' => 'required|email|max:50',
            'subject' => 'required|max:50',
            'phone' => 'required|max:15',
            'message' => 'required|max:500',
            'g-recaptcha-response' => 'required|captcha',
        ];
        $customMessages = [
            'required' => 'Campo requerido.',
            'max' => ':attribute supera el maximo de caracteres.',
            'g-recaptcha-response' => [
                'required' => 'Verifica que no eres un robot',
                'captcha' => 'Error en la verificacion, contacta con el administrador del sitio.',
            ],
        ];
        $this->validate($request, $rules, $customMessages);
        try {

            //            $save = new ContactUs;
            //            $save->name = $request->name;
            //            $save->email = $request->email;
            //            $save->subject = $request->subject;
            //            $save->message = $request->message;
            //            $save->save();

            $this->mail(request('name'), request('email'), request('subject'), request('phone'), request('message'));
            return redirect()->route('contacto web success');
        } catch (Exception $e) {
            return abort(404);
        }
    }

    public function mail($name, $email, $subject, $phone, $message) {
        $data = array(
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
            'phone' => $phone,
            'bodyMessage' => $message,
        );

        Mail::send('email_template', $data, function ($message) use ($data) {
            $message->from(env('MAIL_USERNAME'));
            $message->to(env('MAIL_CONTACT'));
            $message->subject($data['subject']);
        });

        return true;
    }
}
