<?php

namespace App\Http\Controllers;

use App\Http\Resources\CityResource;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


class CityController extends Controller {
    public function post(Request $request) {
        if (request('id') != null && request('id') > 0) {
            return CityResource::collection(City::where('id', request('id'))->where('active', 1)->get());
        }
        if (request('region_id') != null && request('region_id') > 0) {
            return CityResource::collection(City::where('region_id', request('region_id'))->where('active', 1)->get());
        }
        if (request('name') != null) {
            return CityResource::collection(City::where('name', request('name'))->where('active', 1)->get());
        }
    }

    public function get(Request $request) {
        return CityResource::collection(City::get());
    }

    public function patch(Request $request) {

        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        $rules = [
            'id' => 'required|integer|exists:category,id',
            'price' => 'required|integer',
            'active' => 'required|max:4',
        ];
        $customMessages = [
            'required' => ':attribute campo requerido.',
            'exists' => ':attribute ya existe.',
            'integer' => ':attribute debe ser texto.',
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            try {
                $city = City::find(request('id'));
                $city->dispatch_price = request('price');
                $city->active = request('active');
                $city->save();
                return [
                    'status' => 'true',
                    'message' => 'Ciudad actualizada',
                    'cities' => CityResource::collection(City::get()),
                ];
            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Error interno',
                ];
            }
        }
    }
}
