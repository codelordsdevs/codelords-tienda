<?php

namespace App\Http\Controllers;

use App\Models\Sale;
use App\Models\Product;
use App\Models\Payment;

use Illuminate\Http\Request;

class PaymentController extends Controller {
    protected $apiKey;
    protected $secretKey;

    public function __construct() {
        $this->apiKey = env("API_KEY_FLOW", "409FF9CF-C526-4821-BF75-853C03DAL36A");
        $this->secretKey = env("SECRET_KEY_FLOW", "cb29747651d5480eb67db29f39e92fdb41bdc80b");
    }

    public function getTypes() {
        return Payment::all();
    }

    public function sendRequestFlow($service, $params, $method = "GET") {
        $method = strtoupper($method);
        $url = env("API_URL_FLOW", "https://sandbox.flow.cl/api") . "/" . $service;
        $params = array("apiKey" => $this->apiKey) + $params;
        $params["s"] = $this->signFlow($params);
        if ($method == "GET") {
            $response = $this->httpGetFlow($url, $params);
        } else {
            $response = $this->httpPostFlow($url, $params);
        }
        $output = json_decode($response['output'], true);
        if (isset($output['code'])) {
            $returnResponse = array(
                "status" => false,
                "response" => json_decode($response['output'], true),
            );
            return $returnResponse;
        } else {
            if (isset($response["info"])) {
                $code = $response["info"]["http_code"];
                if (!in_array($code, array("200", "400", "401"))) {
                    $returnResponse = array(
                        "status" => false,
                        "response" => json_decode($response['output'], true),
                    );
                    return $returnResponse;
                } else {
                    $returnResponse = array(
                        "status" => true,
                        "response" => json_decode($response['output'], true),

                    );
                    return $returnResponse;
                }
            }
        }
    }

    public function setKeysFlow($apiKey, $secretKey) {
        $this->apiKey = $apiKey;
        $this->secretKey = $secretKey;
    }

    private function signFlow($params) {
        $keys = array_keys($params);
        sort($keys);
        $toSign = "";
        foreach ($keys as $key) {
            $toSign .= $key . $params[$key];
        }
        if (!function_exists("hash_hmac")) {
            // throw new Exception("function hash_hmac not exist", 1);
            return 'funcion hmac no existe';
        }
        return hash_hmac('sha256', $toSign, $this->secretKey);
    }

    private function httpGetFlow($url, $params) {
        $url = $url . "?" . http_build_query($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $output = curl_exec($ch);
        if ($output === false) {
            $error = curl_error($ch);
            //throw new Exception($error, 1);
            return 0;
        }
        $info = curl_getinfo($ch);
        curl_close($ch);
        return array("output" => $output, "info" => $info);
    }

    private function httpPostFlow($url, $params) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $output = curl_exec($ch);
        if ($output === false) {
            $error = curl_error($ch);
            //throw new Exception($error, 1);
            return 0;
        }
        $info = curl_getinfo($ch);
        curl_close($ch);
        return array("output" => $output, "info" => $info);
    }


    public function createPaymentFlow($params) {
        $serviceName = "payment/create";

        try {
            // Instancia la clase FlowApi
            // Ejecuta el servicio
            $response = $this->sendRequestFlow($serviceName, $params, "POST");
            if ($response['status'] == true) {

                $redirect = $response["response"]["url"] . "?token=" . $response["response"]["token"];

                return [
                    'status' => 'true',
                    'url' => $redirect,
                ];
            } else {
                return [
                    'status' => 'false',
                    'response' => $response,
                ];
            }
            //Prepara url para redireccionar el browser del pagador
        } catch (Exception $e) {
            return [
                'status' => 'false',
                'message' => $e->getCode() . " - " . $e->getMessage(),
            ];
        }
    }

    public function redirectPaymentFlow(Request $request) {
        try {
            return redirect(env('APP_FRONT_URL', 'http://localhost:3000') . '/orderstatus/' . request()->segment(2));
        } catch (Exception $e) {
            echo "Error: " . $e->getCode() . " - " . $e->getMessage();
        }
    }

    public function confirmPaymentFlow(Request $request) {
        try {
            if (!request("token") != NULL) {
                // throw new Exception("No se recibio el token", 1);
                echo 'no se recibio token';
            }
            $token = request('token');
            $params = array(
                "token" => $token
            );
            $serviceName = "payment/getStatus";
            $response = $this->sendRequestFlow($serviceName, $params, "GET");


            //Actualiza los datos en su sistema
            if ($response['status'] == 2) {
                $sale = Sale::with(['sale_detail.product', 'user'])->find($response['response']['commerceOrder']);
                //validamos si la venta pertenece a flow
                if ($sale->payment_id != 1 || $sale->sale_state_id != 2) {
                    return 'Venta no valida';
                }
                for ($i = 0; $i < sizeOf($sale->sale_detail); $i++) :
                    if ($sale['sale_detail'][$i]->product->stock > 0 && ($sale['sale_detail'][$i]->product->stock > $sale['sale_detail'][$i]->qty)) :
                        $product = Product::find($sale['sale_detail'][$i]->product_id);
                        $product->stock = $product->stock - $sale['sale_detail'][$i]->qty;
                        $product->save();
                    endif;
                endfor;
                $sale->sale_state_id = 3;
                $sale->save();

                $this->sendConfirmMail($sale->id);

            } else {
                return 'no ok';
            }
        } catch (Exception $e) {
            echo "Error: " . $e->getCode() . " - " . $e->getMessage();
        }
    }

    public function sendConfirmMail($idVenta){
        $sale = Sale::with(['sale_detail.product', 'user', 'city.region'])->find($idVenta);

        $newArray = [];
        $total = 0;
        for($i = 0; $i < count($sale['sale_detail']); $i++){
            $newObj = (object)[];
            $newObj->producto = $sale['sale_detail'][$i]['product']['name'];
            $newObj->qty = $sale['sale_detail'][$i]['qty'];
            $newObj->precioUnitario = $sale['sale_detail'][$i]['unit_price'];
            $newObj->precio = $sale['sale_detail'][$i]['total_price'];
            $total = $total + $newObj->precio;
            array_push($newArray, $newObj);
        }

        $details = [
            'region' => $sale['city']['region']['name'],
            'city' => $sale['city']['name'],
            'address' => $sale['address'],
            'ventas' => $newArray,
            'transactionID' => $sale->id,
            'clientName' => $sale->receiver_name,
            'total' => $total
        ];

        // print_r($details);

        $emailAdmin = env("MAIL_ADMIN", null);

        if($emailAdmin != null && $emailAdmin != ""){
            $var1 =  \Mail::to($emailAdmin)->send(new \App\Mail\confirmEmail($details));
        }

        $var1 =  \Mail::to($sale->email)->send(new \App\Mail\confirmEmail($details));
    }
}
