<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Validator;
use App\Models\Product_image;
use Illuminate\Support\Facades\db;

class ProductController extends Controller {
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['get', 'post', 'options']]);
    }

    public function get() {
        return ProductResource::collection(Product::whereNull('product_id')->with('product_images')->get());
    }

    public function post(Request $request) {
        if (request('id') != null && request('id') > 0) {
            return ProductResource::collection(Product::whereNull('product_id')->where('id', request('id'))->with('product_images')->with('subproducts.product_images')->with('subproducts.subproducts.product_images')->get());
        }
    }

    public function put(Request $request) {

        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        $rules = [
            'name' => 'required|max:100|unique:product,name',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'category_id' => 'required|integer|max:20',
            'featured' => 'required|max:4',
            'description' => 'required|max:1000',
            'special_price' => 'integer',
        ];
        $customMessages = [
            'required' => ':attribute campo requerido.',
            'max' => ':attribute supera cantidad maxima de caracteres.',
            'unique' => ':attribute ya existe.',
            'integer' => ':attribute debe ser numerico.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            $product = new Product();
            try {
                $product->name = request('name');
                $product->price = request('price');
                $product->stock = request('stock');
                $product->special_price = request('special_price');
                $product->category_id = request('category_id');
                $product->featured = request('featured');
                $product->description = request('description');
                $product->special_price_end_date = request('special_price_end_date');
                $product->product_type_id = 1;
                $product->save();
                return [
                    'status' => 'true',
                    'message' => 'Producto creado',
                    'products' => ProductResource::collection(Product::whereNull('product_id')->with('product_images')->get())
                ];
            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Error interno',
                ];
            }
        }
    }

    public function patch(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        if (request('id') != null && request('id') > 0) {
            $product = Product::where('id', request('id'))->get();
            if (($product[0] != null)) {
                $rules = [
                    'name' => 'required|max:100|unique:product,name',
                    'price' => 'required|integer',
                    'stock' => 'required|integer',
                    'category_id' => 'required|integer|max:20',
                    'featured' => 'required|max:4',
                    'description' => 'required|max:1000',
                    'special_price' => 'integer',
                ];
                $customMessages = [
                    'required' => ':attribute campo requerido.',
                    'max' => ':attribute supera cantidad maxima de caracteres.',
                    'unique' => ':attribute ya existe.',
                    'integer' => ':attribute debe ser numerico.'
                ];
                $validator = Validator::make($request->all(), $rules, $customMessages);
                if ($validator->fails()) {
                    return $validator->messages();
                } else {
                    try {
                        if ((request('name') != null) && (strlen(request('name')) > 0)) {
                            $product[0]->name = request('name');
                        }
                        $product[0]->price = request('price');
                        $product[0]->stock = request('stock');
                        $product[0]->special_price = request('special_price');
                        $product[0]->category_id = request('category_id');
                        $product[0]->featured = request('featured');
                        $product[0]->description = request('description');
                        $product[0]->special_price_end_date = request('special_price_end_date');
                        $product[0]->save();
                        return [
                            'status' => 'true',
                            'message' => 'Producto actualizado',
                            'products' => ProductResource::collection(Product::whereNull('product_id')->with('product_images')->get())

                        ];
                    } catch (Exception $ex) {
                        //poner esto en un log
                        return [
                            'status' => 'false',
                            'message' => 'Error interno',
                        ];
                    }
                }
            } else {
                return [
                    'status' => 'false',
                    'message' => 'Producto no encontrado',
                ];
            }
        }
        return [
            'status' => 'false',
            'message' => 'Request sin ID',
        ];
    }

    public function delete(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        if (request('id') != null && request('id') > 0) {
            $product = Product::find(request('id'));
            if (($product != null) && ($product->stock == 0)) {
                try {
                    $product->delete();
                    return [
                        'status' => 'true',
                        'message' => 'Producto eliminado',
                        'products' => ProductResource::collection(Product::whereNull('product_id')->with('product_images')->get())

                    ];
                } catch (Exception $ex) {
                    //poner esto en un log
                    return [
                        'status' => 'false',
                        'message' => 'Error interno',
                    ];
                }
            } else {
                return [
                    'status' => 'false',
                    'message' => 'Producto no se puede eliminar, tiene stock mayor a 0',
                ];
            }
        }
        return [
            'status' => 'false',
            'message' => 'Request sin ID',
        ];
    }

    public function options(Request $request) {
        //Inicio Obtener productos destacados
        if (((request('featured') != null) && (request('featured') == 1)) && ((request('limit') != null) && (request('limit') > 0))) {
            return ProductResource::collection(Product::whereNull('product_id')->where('product_type_id', 1)->where('featured', 1)->with('product_images')->take(request('limit'))->get());
        }
        if ((request('featured') != null) && (request('featured') == 1)) {
            return ProductResource::collection(Product::whereNull('product_id')->where('product_type_id', 1)->where('featured', 1)->get());
        }
        //Fin Obtener productos destacados
        //Inicio Obtener productos en oferta
        if (((request('discount') != null) && (request('discount') == 1)) && ((request('limit') != null) && (request('limit') > 0))) {
            return ProductResource::collection(Product::whereNull('product_id')->where('product_type_id', 1)->where('special_price_end_date', '>', \DB::raw('NOW()'))->with('product_images')->take(request('limit'))->get());
        }
        if ((request('discount') != null) && (request('discount') == 1)) {
            return ProductResource::collection(Product::whereNull('product_id')->where('product_type_id', 1)->where('special_price_end_date', '>', \DB::raw('NOW()'))->with('product_images')->get());
        }
        //Inicio Obtener productos en oferta
        //Inicio Obtener productos nuevos
        if (((request('news') != null) && (request('news') == 1)) && ((request('limit') != null) && (request('limit') > 0))) {
            return ProductResource::collection(Product::whereNull('product_id')->where('product_type_id', 1)->with('product_images')->orderBy('created_at', 'desc')->take(request('limit'))->get());
        }
        if ((request('news') != null) && (request('news') == 1)) {
            return ProductResource::collection(Product::whereNull('product_id')->where('product_type_id', 1)->with('product_images')->orderBy('created_at', 'desc')->get());
        }
        //Inicio Obtener productos nuevos
        //Inicio Busqueda productos nuevos
        if ((request('search') != null) && (request('limit') != null) && (request('limit') > 0)) {

            return ProductResource::collection(Product::whereNull('product_id')->where('product_type_id', 1)->where('name', 'LIKE', '%' . request('search') . '%')
                ->orWhere('id', 'LIKE', '%' . request('search') . '%')
                ->orWhere('description', 'LIKE', '%' . request('search') . '%')
                ->take(request('limit'))
                ->get());
        }
        if ((request('search') != null)) {

            return ProductResource::collection(Product::whereNull('product_id')->where('product_type_id', 1)->where('name', 'LIKE', '%' . request('search') . '%')
                ->orWhere('id', 'LIKE', '%' . request('search') . '%')
                ->orWhere('description', 'LIKE', '%' . request('search') . '%')
                ->get());
        }
        //Fin Busqueda productos nuevos
        //Inicio Obtener productos por categoria y rango de precio
        if ((request('price_from') != null)
            && (is_numeric(request('price_from')))
            && (request('price_to') != null)
            && (request('price_to') > 0)
            && (is_numeric(request('price_to')))
            && (request('category_id') != null)
        ) {
            $array = explode(',', request('category_id'));
            return ProductResource::collection(Product::whereNull('product_id')->where('product_type_id', 1)->whereIn('category_id', $array)
                ->where(function ($query) {
                    $query->wherebetween('price', [request('price_from'), request('price_to')])
                        ->orwherebetween('special_price', [request('price_from'), request('price_to')])
                        ->where('special_price_end_date', '>', \DB::raw('NOW()'));
                })->get());
            // return ProductResource::collection(Product::wherebetween('price', [request('price_from'), request('price_to')])
            // ->orwherebetween('special_price', [request('price_from'), request('price_to')])
            // ->where('special_price_end_date','>', \DB::raw('NOW()'))
            // ->where('category_id', request('category_id'))->get());
        }
        //Fin Obtener productos por categoria y rango de precio
        //Inicio Obtener productos por categoria
        if ((request('category_id') != null)) {
            $array = explode(',', request('category_id'));
            return ProductResource::collection(Product::whereNull('product_id')->where('product_type_id', 1)->whereIn('category_id', $array)->get());
        }
        //Fin Obtener productos por categoria
        //Inicio Obtener productos por rango de precio
        if ((request('price_from') != null)
            && (is_numeric(request('price_from')))
            && (request('price_to') != null)
            && (request('price_to') > 0)
            && (is_numeric(request('price_to')))
        ) {
            return ProductResource::collection(Product::whereNull('product_id')->where('product_type_id', 1)->wherebetween('price', [request('price_from'), request('price_to')])
                ->orwherebetween('special_price', [request('price_from'), request('price_to')])
                ->where('special_price_end_date', '>', \DB::raw('NOW()'))->get());
        }
        //Fin Obtener productos por rango de precio
        return [
            'status' => 'error',
            'message' => 'not match',
        ];
    }
}
