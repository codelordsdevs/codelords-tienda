<?php

namespace App\Http\Controllers;

use App\Http\Resources\SaleResource;
use App\Models\Sale;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\SaleDetailResource;
use App\Models\SaleDetail;
use App\Models\Cart;
use App\Http\Resources\CartResource;
use App\Http\Resources\ShipmentResource;
use App\Models\DispatchAdress;
use Illuminate\Support\Facades\URL;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\Config;
use App\Http\Resources\ConfigResource;


class SaleController extends Controller {

    public function __construct() {
        $this->middleware('auth:api', ['except' => ['post', 'put', 'delete']]);
    }

    public function get(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador
        return SaleResource::collection(Sale::get());
    }

    public function post(Request $request) {
        if (request('id') != null && request('id') > 0) {
            return SaleResource::collection(Sale::where('id', request('id'))->get());
        }
    }


    public function put(Request $request) {
        $rules = [
            'payment_id' => [
                'required',
                'integer',
            ]
        ];
        $customMessages = [
            'required' => ':attribute campo requerido.',
            'max' => ':attribute supera cantidad maxima de caracteres.',
            'unique' => ':attribute ya existe.',
            'integer' => ':attribute debe ser numerico.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            //If you don't have an id in the request, a new one is added
            try {

                if (auth()->user() != null) {
                    $CartRequest = CartResource::collection(Cart::where('user_id', auth()->user()->id)->where('selected', 1)->get());
                    if ($CartRequest) {
                        $Sale = new Sale();
                        $Sale->user_id = auth()->user()->id;
                        $Sale->payment_id = request('payment_id');
                        $Sale->sale_state_id = 2;
                        $Sale->address = request('address');
                        $Sale->city_id = request('city_id');
                        $Sale->phone = request('phone');
                        $Sale->postal_code = request('postal_code');
                        $Sale->receiver_name = request('receiver_name');
                        $Sale->email = request('email');
                        $Sale->pickup = request('pickup');
                        $Sale->save();
                        $Sale->id;

                        $total = 0;


                        for ($k = 0; $k < count($CartRequest); $k++) {

                            if ($CartRequest[$k]->selected == 1) {
                                $sale_id = $Sale->id;
                                $qty = $CartRequest[$k]->qty;
                                $unit_price = $CartRequest[$k]->product->price;
                                $total_price = $unit_price * $qty;
                                $product_id = $CartRequest[$k]->product->id;

                                $SaleDetail = new SaleDetail();
                                $SaleDetail->sale_id = $sale_id;
                                $SaleDetail->qty = $qty;
                                $SaleDetail->unit_price = $unit_price;
                                $SaleDetail->total_price = $total_price;
                                $SaleDetail->product_id = $product_id;

                                $total = $total_price + $total; //retorna el total de la venta.

                                $SaleDetail->save();

                                $cart = Cart::where('id', $CartRequest[$k]->id)->get();
                                if (($cart[0] != null)) {
                                    $cart[0]->delete();
                                }
                            }
                        }

                        if (request('save_shipment') == "1") {

                            $shipment = new DispatchAdress();
                            $shipment->user_id = auth()->user()->id;
                            $shipment->postal_code = request('postal_code');
                            $shipment->name = request('name');
                            $shipment->address = request('address');
                            $shipment->city_id = request('city_id');
                            $shipment->phone = request('phone');
                            $shipment->receiver_name = request('receiver_name');
                            $shipment->email = request('email');
                            $shipment->save();
                        }

                        if (request('payment_id') == 1) {
                            //Para datos opcionales campo "optional" prepara un arreglo JSON
                            $optional = array(
                                "user_id" => auth()->user()->id,
                                "sale_id" => $Sale->id,
                                "user_name" => auth()->user()->name,
                                "user_email" => auth()->user()->email,
                            );
                            $optional = json_encode($optional);
                            //Prepara el arreglo de datos
                            $params = array(
                                "commerceOrder" => $Sale->id,
                                "subject" => $Sale->id . "-" . date("Ymd"),
                                "currency" => "CLP",
                                "amount" => intval($total),
                                "email" => $Sale->email,
                                "paymentMethod" => 9,
                                "urlConfirmation" => URL::to('/payment_confirm'),
                                "urlReturn" =>  URL::to('/flow_redirect'),
                                "optional" => $optional
                            );
                            //Define el metodo a usar
                            return \App('App\Http\Controllers\PaymentController')->createPaymentFlow($params);
                        } else if (request('payment_id') == 2) {
                            $this->sendDepositMail($Sale, $SaleDetail);

                            return [
                                'status' => 'true',
                                'message' => 'Sale added successfully',
                                'SaleID' => $Sale->id,
                                'Total' => $total,
                            ];
                        }
                    }
                } else {
                    $Sale = new Sale();
                    // $Sale->user_id = auth()->user()->id;
                    $Sale->payment_id = request('payment_id');
                    $Sale->sale_state_id = 2;
                    $Sale->address = request('address');
                    $Sale->city_id = request('city_id');
                    $Sale->phone = request('phone');
                    $Sale->postal_code = request('postal_code');
                    $Sale->receiver_name = request('receiver_name');
                    $Sale->email = request('email');
                    $Sale->pickup = request('pickup');
                    $Sale->save();
                    $Sale->id;

                    $total = 0;

                    $CartRequest = json_decode(request('products'));
                    for ($k = 0; $k < count($CartRequest); $k++) {

                        if ($CartRequest[$k]->selected == 1) {
                            $sale_id = $Sale->id;
                            $qty = $CartRequest[$k]->qty;
                            $unit_price = $CartRequest[$k]->product->price;
                            $total_price = $unit_price * $qty;
                            $product_id = $CartRequest[$k]->product->id;

                            $SaleDetail = new SaleDetail();
                            $SaleDetail->sale_id = $sale_id;
                            $SaleDetail->qty = $qty;
                            $SaleDetail->unit_price = $unit_price;
                            $SaleDetail->total_price = $total_price;
                            $SaleDetail->product_id = $product_id;

                            $total = $total_price + $total; //retorna el total de la venta.

                            $SaleDetail->save();
                        }
                    }

                    if (request('save_shipment') == "1") {

                        $shipment = new DispatchAdress();
                        $shipment->user_id = auth()->user()->id;
                        $shipment->postal_code = request('postal_code');
                        $shipment->name = request('name');
                        $shipment->address = request('address');
                        $shipment->city_id = request('city_id');
                        $shipment->phone = request('phone');
                        $shipment->receiver_name = request('receiver_name');
                        $shipment->email = request('email');
                        $shipment->save();
                    }

                    if (request('payment_id') == 1) {
                        //Para datos opcionales campo "optional" prepara un arreglo JSON
                        $optional = array(
                            "user_id" => 0,
                            "sale_id" => $Sale->id,
                            "user_name" => request('name'),
                            "user_email" => request('email'),
                        );
                        $optional = json_encode($optional);
                        //Prepara el arreglo de datos
                        $params = array(
                            "commerceOrder" => $Sale->id,
                            "subject" => $Sale->id . "-" . date("Ymd"),
                            "currency" => "CLP",
                            "amount" => intval($total),
                            "email" => $Sale->email,
                            "paymentMethod" => 9,
                            "urlConfirmation" => URL::to('/payment_confirm'),
                            "urlReturn" =>  URL::to('/flow_redirect/' . $Sale->id),
                            "optional" => $optional
                        );
                        //Define el metodo a usar
                        return \App('App\Http\Controllers\PaymentController')->createPaymentFlow($params);
                    } else if (request('payment_id') == 2) {
                        // $config = ConfigResource::collection(Config::all());

                        // $body = "<b>Banco</b>:&nbsp;" . $config[0]["deposit_bank"] . "</br>
                        //     <b>Tipo de cuenta</b>:&nbsp;" . $config[0]["deposit_account_type"] . "</br>
                        //     <b>Numero de cuenta</b>:&nbsp;" . $config[0]["deposit_number"] . "</br>
                        //     <b>Rut</b>:&nbsp;" . $config[0]["deposit_rut"] . "</br>
                        //     <b>Nombre titular</b>:&nbsp;" . $config[0]["deposit_name"] . "</br>";

                        // $details = [
                        //     'title' => 'Para finalizar el pago de su compra, debe realizar el pago por transferencia',
                        //     'banc' =>  $config[0]["deposit_bank"],
                        //     'accountType' => $config[0]["deposit_account_type"],
                        //     'accountNumber' => $config[0]["deposit_number"],
                        //     'rut' => $config[0]["deposit_rut"],
                        //     'name' => $config[0]["deposit_name"],
                        //     'transactionID' => $Sale->id,
                        //     'clientName' => $Sale->receiver_name,
                        //     'total' => $SaleDetail->total_price
                        // ];

                        // $var1 =  \Mail::to($Sale->email)->send(new \App\Mail\depositEmail($details));

                        $this->sendDepositMail($Sale, $SaleDetail);
                        // $this->sendConfirmMail($Sale->id);

                        return [
                            'status' => 'true',
                            'message' => 'Sale added successfully',
                            'SaleID' => $Sale->id,
                            'Total' => $total,
                        ];
                    }
                }
            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Internal Error',
                ];
            }
        }
    }

    public function putworker(Request $request) {
        $rules = [
            'payment_id' => [
                'required',
                'integer',
            ]
        ];
        $customMessages = [
            'required' => ':attribute campo requerido.',
            'max' => ':attribute supera cantidad maxima de caracteres.',
            'unique' => ':attribute ya existe.',
            'integer' => ':attribute debe ser numerico.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            //If you don't have an id in the request, a new one is added
            try {
                $cart = request('products');
                if (!count($cart) > 0) {
                    return [
                        'status' => false,
                        'message' => 'Debe agregar por lo menos 1 producto a la venta',
                    ];
                }
                $Sale = new Sale();
                if (request('email') != '') {
                    $Sale->user_id = User::where('email', request('email'))->first()->id;
                }
                $Sale->vendor_id = auth()->user()->id;
                $Sale->sale_state_id = 2;
                $Sale->address = request('address') != "" ? $request['address'] : "N/A";
                $Sale->address_extrainfo = request('address_extrainfo') != "" ? $request['address_extrainfo'] : "N/A";
                $Sale->city_id = request('city_id') != null ? $request['city_id'] : null;
                $Sale->phone = request('phone')  != '' ? request('phone') : '';
                $Sale->postal_code = request('posta_code')  != '' ? request('posta_code') : 0;
                $Sale->receiver_name = request('client_name')  != '' ? request('client_name') : '';
                $Sale->email = request('email') != "" ? $request['email'] : "N/A";
                $Sale->pickup = request('pickup');
                $Sale->save();
                $total = 0;
                for ($k = 0; $k < count($cart); $k++) {
                    $sale_id = $Sale->id;
                    $qty = $cart[$k]["qty"];
                    $this_product = Product::find($cart[$k]["value"]);
                    $unit_price = $this_product->price;
                    $total_price = $unit_price * $qty;
                    $product_id = $this_product->id;

                    $SaleDetail = new SaleDetail();
                    $SaleDetail->sale_id = $sale_id;
                    $SaleDetail->qty = $qty;
                    $SaleDetail->unit_price = $unit_price;
                    $SaleDetail->total_price = $total_price;
                    $SaleDetail->product_id = $product_id;
                    $total = $total_price + $total; //retorna el total de la venta.
                    $SaleDetail->save();
                }

                if (request('save_shipment') == "1") {
                    $shipment = new DispatchAdress();
                    $shipment->user_id = auth()->user()->id;
                    $shipment->postal_code = request('postal_code');
                    $shipment->name = request('name');
                    $shipment->address = request('address');
                    $shipment->city_id = request('city_id');
                    $shipment->phone = request('phone');
                    $shipment->receiver_name = request('receiver_name');
                    $shipment->email = request('email');
                    $shipment->save();
                }

                return [
                    'status' => true,
                    'message' => 'venta creada con exito',
                    'sale_id' => $Sale->id,
                ];

            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Internal Error',
                ];
            }
        }
    }

    public function options(Request $request) {

        // Inicio obtener pedidos de usuario

        if ((request('my_orders') != null) && (request('my_orders') == 1)) {
            return SaleResource::collection(Sale::with('sale_detail.product')->with('sale_state')->where('user_id', request('user_id'))->get());
        }

        // Fin obtener pedidos de usuario

        // Inicio obtener pedidos por despachar

        if ((request('to_dispatch') != null) && (request('to_dispatch') == 1)) {
            return SaleResource::collection(Sale::with('sale_detail.product')->with('sale_state')->where('user_id', request('user_id'))
                ->where('pickup', 0)->where('sale_state_id', 4)->get());
        }

        // Fin obtener pedidos por despachar

        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        //Inicio Obtener cola de pedidos
        if ((request('queue') != null) && (request('queue') == 1)) {
            return SaleResource::collection(Sale::with('sale_detail.product')->where('pickup', 1)->where('sale_state_id', 3)->get());
        }
        //Fin Obtener cola de pedidos

        //Inicio Obtener ventas con retiro
        if ((request('pickups') != null) && (request('pickups') == 1)) {
            return SaleResource::collection(Sale::with('sale_detail.product')->where('pickup', 1)->where('sale_state_id', 4)->get());
        }
        //Fin Obtener ventas con retiro

        //Inicio confirmar ventas con retiro
        if ((request('confirm_pickup') != null) && (request('confirm_pickup') == 1)) {
            $rules = [
                'id' => [
                    'required',
                    'integer',
                    'exists:sale,id'
                ]
            ];
            $customMessages = [
                'required' => ':attribute campo requerido.',
                'max' => ':attribute supera cantidad maxima de caracteres.',
                'unique' => ':attribute ya existe.',
                'integer' => ':attribute debe ser numerico.'
            ];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                return $validator->messages();
            } else {
                $sale = Sale::find(request('id'));
                $sale->sale_state_id = 4;
                $sale->save();
                return [
                    'status' => 'true',
                    'pickups' => SaleResource::collection(Sale::with('sale_detail')->where('pickup', 1)->where('sale_state_id', 4)->get()),
                ];
            }
        }
        //Fin confirmar ventas con retiro

        //Inicio confirmar ventas queue
        if ((request('confirm_queue') != null) && (request('confirm_queue') == 1)) {
            $rules = [
                'id' => [
                    'required',
                    'integer',
                    'exists:sale,id'
                ]
            ];
            $customMessages = [
                'required' => ':attribute campo requerido.',
                'max' => ':attribute supera cantidad maxima de caracteres.',
                'unique' => ':attribute ya existe.',
                'integer' => ':attribute debe ser numerico.'
            ];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                return $validator->messages();
            } else {
                $sale = Sale::find(request('id'));
                $sale->sale_state_id = 4;
                $sale->save();
                return [
                    'status' => 'true',
                    'pickups' => SaleResource::collection(Sale::with('sale_detail')->where('pickup', 1)->where('sale_state_id', 3)->get()),
                ];
            }
        }
        //Fin confirmar venta queue

        //Inicio confirmar pago ventas
        if ((request('payment_confirm') != null) && (request('payment_confirm') == 1)) {
            $rules = [
                'id' => [
                    'required',
                    'integer',
                    'exists:sale,id'
                ],
                'code' => [
                    'required',
                    'string',
                    'unique:sale,transaction_code'
                ]
            ];
            $customMessages = [
                'required' => ':attribute campo requerido.',
                'max' => ':attribute supera cantidad maxima de caracteres.',
                'unique' => ':attribute ya existe.',
                'integer' => ':attribute debe ser numerico.'
            ];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                return $validator->messages();
            } else {
                $sale = Sale::find(request('id'));
                $sale->sale_state_id = 3;
                $sale->transaction_code = request('code');
                $sale->save();
                return [
                    'status' => 'true',
                    'sales' => SaleResource::collection(Sale::get()),
                ];
            }
        }
        //Fin confirmar pago ventas
    }

    public function sendDepositMail($Sale, $SaleDetail){
        $config = ConfigResource::collection(Config::all());
        //return $config[0]["deposit_bank"];

        $body = "<b>Banco</b>:&nbsp;" . $config[0]["deposit_bank"] . "</br>
            <b>Tipo de cuenta</b>:&nbsp;" . $config[0]["deposit_account_type"] . "</br>
            <b>Numero de cuenta</b>:&nbsp;" . $config[0]["deposit_number"] . "</br>
            <b>Rut</b>:&nbsp;" . $config[0]["deposit_rut"] . "</br>
            <b>Nombre titular</b>:&nbsp;" . $config[0]["deposit_name"] . "</br>";

        $details = [
            'title' => 'Para finalizar el pago de su compra, debe realizar el pago por transferencia',
            'banc' =>  $config[0]["deposit_bank"],
            'accountType' => $config[0]["deposit_account_type"],
            'accountNumber' => $config[0]["deposit_number"],
            'rut' => $config[0]["deposit_rut"],
            'name' => $config[0]["deposit_name"],
            'transactionID' => $Sale->id,
            'clientName' => $Sale->receiver_name,
            'total' => $SaleDetail->total_price
        ];

        $var1 =  \Mail::to($Sale->email)->send(new \App\Mail\depositEmail($details));
    }
}
