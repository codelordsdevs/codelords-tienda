<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Coupon;
use App\Models\User;
use App\Http\Resources\CouponResource;
use Illuminate\Support\Facades\Validator;

class CouponController extends Controller {
    public function __construct() {
        $this->middleware('auth:api', ['except' => []]);
    }

    public function get(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador
        return CouponResource::collection(Coupon::with('product')->with('category')->with('user')->get());
    }

    public function post(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        $rules = [
            'code' => 'required|exists:coupon,code',
            'user_id' => 'nullable|integer|exists:user,id',
        ];

        $customMessages = [
            'required' => ':attribute campo requerido.',
            'max' => ':attribute supera cantidad maxima de caracteres.',
            'unique' => ':attribute ya existe.',
            'integer' => ':attribute debe ser numerico.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            $coupon = Coupon::where('code', request('code'))->first();
            if ($coupon->user_id != null && $coupon->user_id != request('user_id')) {
                return [
                    'status' => 'false',
                    'message' => 'Cupon no valido para este usuario',
                ];
            }
            return [
                'status' => 'true',
                'coupon' => $coupon,
            ];
        }
    }

    public function put(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        $rules = [
            'product_id' => 'nullable|integer|exists:product,id',
            'category_id' => 'nullable|integer|exists:category,id',
            'email' => 'nullable|email|exists:user,email',
            'qty' => 'required|integer',
            'is_percentage' => 'required|bool',
            'discount' => 'required|integer',
            'expired_at' => 'required|date',
        ];

        $customMessages = [
            'required' => ':attribute campo requerido.',
            'max' => ':attribute supera cantidad maxima de caracteres.',
            'unique' => ':attribute ya existe.',
            'integer' => ':attribute debe ser numerico.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            //validar el stock de los productos antes de esto
            try {
                $coupon = new Coupon();
                if (request('product_id') != null && request('product_id') != 0) {
                    $coupon->product_id = request('product_id');
                }
                if (request('email') != null) {
                    $coupon->user_id = User::where('email', request('email'))->first()->id;
                }
                if (request('category_id') != null && request('category_id') != "0") {
                    $coupon->category_id = request('category_id');
                }
                $coupon_code = "";
                do {
                    $coupon_code = $this->generateRandomString(6);
                } while (!Coupon::where('code', $coupon_code)->first() === null);
                $coupon->code = $coupon_code;
                $coupon->qty = request('qty');
                $coupon->discount = request('discount');
                $coupon->is_percentage = request('is_percentage');
                $coupon->expired_at = request('expired_at');
                $coupon->save();

                return [
                    'status' => 'true',
                    'message' => 'Cupon creado con exito',
                    'coupons' => CouponResource::collection(Coupon::with('product')->with('category')->with('user')->get()),
                ];
            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Error interno',
                ];
            }
        }
    }

    public function delete(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        $rules = [
            'id' => 'required|integer|exists:coupon,id',
        ];
        $customMessages = [
            'required' => ':attribute campo requerido.',
            'exists' => ':attribute ya existe.',
            'integer' => ':attribute debe ser texto.',
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            try {
                $coupon = Coupon::find(request('id'));
                $coupon->delete();
                return [
                    'status' => 'true',
                    'message' => 'Cupon eliminada',
                    'coupons' => CouponResource::collection(Coupon::with('product')->with('user')->get()),
                ];
            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Error interno',
                ];
            }
        }
    }

    public  function generateRandomString($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
