<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserModule;
use App\Models\DispatchAdress;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Verified;
use Illuminate\Notifications\Messages\MailMessage;
use App\Http\Resources\UserResource;

class UserController extends Controller {

	public function __construct() {
		$this->middleware('auth:api', ['except' => ['put', 'options', 'verify']]);
	}

	public function getWorker() {
		return UserResource::collection(User::where('profile_id', 3)->get());
	}

	public function postWorker(Request $request) {
		if (request('id') != null && request('id') > 0) {
			return [
				'user' => new UserResource(User::find(request('id'))),
				'modules' => UserModule::rightjoin('module', function ($join) {
					$join->on('module.id', '=', 'user_module.module_id')->where('user_module.user_id', '=', request('id'));
				})->get(),
			];
		}
	}
	public function postUserByEmail(Request $request) {
		if (request('email') != null && request('email') != '') {
			$user = User::where('email', request('email'))->first();
			if ($user != null) :
				return [
					'status' => true,
					'user' => new UserResource($user),
					'addesses' => DispatchAdress::where('user_id', $user->id)->get(),
				];
			else :
				return [
					'status' => false,
					'message' => 'usuario no encontrado',
				];
			endif;
		}
	}

	public function getUsers() {
		return UserResource::collection(User::where('profile_id', 1)->get());
	}

	public function put(Request $request) {
		$rules = [
			'password' => 'required|min:9',
			'email' => 'required|unique:user,email',
			'name' => 'required',
			'lastname' => 'required',
		];
		$customMessages = [
			'unique' => 'El correo electronico ya esta utilizado',
			'required' => 'Campo requerido.',
			'email' => 'debe ingresar una email valida',
			'min' => 'el largo minimo de la contraseña debe ser de 9 caracteres'
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return [
				'status' => 'false',
				'messages' => $validator->messages(),
			];
		} else {
			try {
				$user = new User();
				$user->name = request('name') . " " . request('lastname');
				$user->email = request('email');
				$user->password = Hash::make(request('password'));
				$user->phone = request('phone');
				$user->save();
				$this->sendVerificationEmail($user);
				return [
					'status' => 'true',
					'message' => 'Usuario creado',
				];
			} catch (Exception $e) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}

	public function putWorker(Request $request) {
		$rules = [
			'password' => 'required|min:9',
			'email' => 'required|unique:user,email',
			'name' => 'required',
			'phone' => 'required',
		];
		$customMessages = [
			'unique' => 'El correo electronico ya esta utilizado',
			'required' => 'Campo requerido.',
			'email' => 'debe ingresar una email valida',
			'min' => 'el largo minimo de la contraseña debe ser de 9 caracteres'
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return [
				'status' => 'false',
				'messages' => $validator->messages(),
			];
		} else {
			try {
				$user = new User();
				$user->name = request('name');
				$user->email = request('email');
				$user->password = Hash::make(request('password'));
				$user->phone = request('phone');
				$user->profile_id = 3;
				$user->save();
				return [
					'status' => 'true',
					'message' => 'Usuario creado',
				];
			} catch (Exception $e) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}

	public function patch(Request $request) {
		$rules = [
			'name' => 'required',
			'lastname' => 'required',
		];
		$customMessages = [
			'unique' => 'El correo electronico ya esta utilizado',
			'required' => 'Campo requerido.',
			'email' => 'debe ingresar una email valida',
			'min' => 'el largo minimo de la contraseña debe ser de 9 caracteres'
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return [
				'status' => 'false',
				'messages' => $validator->messages(),
			];
		} else {
			try {
				$user = User::find(request('id'));
				$user->name = request('name') . " " . request('lastname');
				// $user->password = Hash::make(request('password'));
				$user->phone = request('phone');
				$user->save();
				return [
					'status' => 'true',
					'message' => 'Usuario actualizado',
				];
			} catch (Exception $e) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}

	public function sendVerificationEmail($user) {
		$details = [
			'title' => 'Mail from ItSolutionStuff.com',
			'body' =>  env('APP_FRONT_URL', 'http://127.0.0.1:3000') . '/verify/' . $user->id . '/' . sha1($user->getEmailForVerification())
		];
		return \Mail::to($user->getEmailForVerification())->send(new \App\Mail\verificationEmail($details));
		return (new MailMessage)
			->line('click aqui para verificar tu email')
			->action('Verify', env('APP_FRONT_URL', 'http://127.0.0.1:3000') . '/verify/' . $user->id . '/' . sha1($user->getEmailForVerification()));
	}

	public function options(Request $request) {
		//Inicio verificacion email
		if ((request('verify') != null) && (request('id') > 0) && ((request('hash') != null))) {
			return $this->verify_new($request);
		}
		//fin verificacion email
	}

	public function verify(Request $request) {
		$user = User::find($request->route('id'));
		if (!hash_equals((string) $request->route('hash'), sha1($user->getEmailForVerification()))) {
			throw new AuthorizationException;
		}
		if ($user->markEmailAsVerified())
			event(new Verified($user));
		return [
			'status' => 'true',
			'message' => 'email verificado con exito',
		];
	}

	public function verify_new($request) {
		$rules = [
			'id' => 'required|exists:user,id',
			'hash' => 'required',
		];
		$customMessages = [
			'exists' => 'El usuario no existe',
			'required' => 'Campo requerido.',
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return [
				'status' => 'error',
				'messages' => $validator->messages(),
			];
		} else {

			try {
				$user = User::find(request('id'));
				if (!hash_equals((string) request('hash'), sha1($user->getEmailForVerification()))) {
					throw new AuthorizationException;
				}

				if ($user->markEmailAsVerified()) {
					event(new Verified($user));
					return [
						'status' => 'true',
						'message' => 'email verificado con exito',
					];
				} else {
					return [
						'status' => 'false',
						'message' => 'error al verificar email',
					];
				}
			} catch (Exception $e) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error al verificar email',
				];
			}
		}
	}

	public function changePassword(Request $request) {
		$rules = [
			'oldpassword' => 'required|min:9',
			'newpassword' => 'required|min:9',
			'confirmnewpassword' => 'required|min:9'
		];
		$customMessages = [
			'unique' => 'El correo electronico ya esta utilizado',
			'required' => 'Campo requerido.',
			'email' => 'debe ingresar una email valida',
			'min' => 'el largo minimo de la contraseña debe ser de 9 caracteres'
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return [
				'status' => 'false',
				'messages' => $validator->messages(),
			];
		} else {
			try {
				$user = User::find(request('id'));
				$oldPassword = Hash::make(request('oldpassword'));
				$newPassword = Hash::make(request('newpassword'));
				$confirmNewPassword = Hash::make(request('confirmnewpassword'));

				if (request('newpassword') != request('confirmnewpassword')) {
					return [
						'status' => 'false',
						'message' => 'Contraseñas no coinciden.',
					];
				}

				if (!Hash::check(request('oldpassword'), $user->password)) {
					return [
						'status' => 'false',
						'message' => 'La contraseña antigua no corresponde.',
					];
				}

				$user->password = $newPassword;
				$user->save();
				return [
					'status' => 'true',
					'message' => 'Contraseña actualizada.',
				];
			} catch (Exception $e) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}

	public function optionsWorker(Request $request) {
		if ((request('id') > 0) && (request('set_permission') != null) && (request('set_permission') == 1)) {
			$rules = [
				'module_id' => 'required',
				'state' => 'required|boolean',
				'permission' => 'required',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$UserModule = UserModule::where('user_id', request('id'))->where('module_id', request('module_id'))->first();
					if ($UserModule) :
						switch (request('permission')):
							case 'create':
								$UserModule->create = !$UserModule->create;
								break;
							case 'read':
								$UserModule->read = !$UserModule->read;
								break;
							case 'edit':
								$UserModule->edit = !$UserModule->edit;
								break;
							case 'delete':
								$UserModule->delete = !$UserModule->delete;
								break;
						endswitch;
						$UserModule->save();
						return [
							'status' => 'true',
							'message' => 'Permiso del usuario actualizado correctamente',
							'modules' => UserModule::rightjoin('module', function ($join) {
								$join->on('module.id', '=', 'user_module.module_id')->where('user_module.user_id', '=', request('id'));
							})->get(),
						];
					else :
						$newUserModule = new UserModule();
						$newUserModule->user_id = request('id');
						$newUserModule->module_id = request('module_id');
						switch (request('permission')):
							case 'create':
								$newUserModule->create = request('state');
								$newUserModule->read = false;
								$newUserModule->edit = false;
								$newUserModule->delete = false;
								break;
							case 'read':
								$newUserModule->create = false;
								$newUserModule->read = request('state');
								$newUserModule->edit = false;
								$newUserModule->delete = false;
								break;
							case 'edit':
								$newUserModule->create = false;
								$newUserModule->read = false;
								$newUserModule->edit = request('state');
								$newUserModule->delete = false;
								break;
							case 'delete':
								$newUserModule->create = false;
								$newUserModule->read = false;
								$newUserModule->edit = false;
								$newUserModule->delete = request('state');
								break;
						endswitch;
						$newUserModule->save();
						return [
							'status' => 'true',
							'message' => 'Permiso del usuario actualizado correctamente',
							'modules' => UserModule::rightjoin('module', function ($join) {
								$join->on('module.id', '=', 'user_module.module_id')->where('user_module.user_id', '=', request('id'));
							})->get(),
						];
					endif;
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if ((request('id') > 0) && (request('set_password') != null) && (request('set_password') == 1)) {
			$rules = [
				'password' => 'required|string',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$user = User::find(request('id'));
					$user->password = Hash::make(request('password'));
					$user->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Password del contacto actualizado correctamente',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
		if ((request('id') > 0) && request('set_phone') != null && request('set_phone') == 1) {
			$rules = [
				'phone' => 'required|string',
			];
			$customMessages = [
				'required' => ':attribute campo requerido.',
				'unique' => ':attribute ya existe.',
				'string' => ':attribute debe ser texto.',
			];
			$validator = Validator::make($request->all(), $rules, $customMessages);
			if ($validator->fails()) {
				return $validator->messages();
			} else {
				try {
					$user = User::find(request('id'));
					$user->phone = request('phone');
					$user->save(['timestamps' => FALSE]);
					return [
						'status' => 'true',
						'message' => 'Telefono del contacto actualizado correctamente',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			}
		}
	}
}
