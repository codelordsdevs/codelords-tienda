<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sale;
use App\Models\SaleDetail;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function get(Request $request) {

        $month = date('m');
        $year = date('Y');

        if(request('month') != null && intval(request('month')) > 0 ){
            $month = request('month');
        }
        if(request('year') != null && intval(request('year')) > 0){
            $year = request('year');
        }

        return [
            'total_sales_products' => SaleDetail::whereMonth('updated_at', '=', $month)->whereYear('updated_at', '=', $year)->sum('qty'),
            'total_sales_in_cash' => number_format(SaleDetail::whereMonth('updated_at', '=', $month)->whereYear('updated_at', '=', $year)->sum('total_price'), 0, ".", "."),
            'sales_pending_to_dispatch' => Sale::where('sale_state_id', 4)->whereMonth('updated_at', '=', $month)->whereYear('updated_at', '=', $year)->count(),
            'sales_closed' => Sale::whereIn('sale_state_id', [5, 6])->whereMonth('updated_at', '=', $month)->whereYear('updated_at', '=', $year)->count(),
            'sales_confirm_pending' => Sale::where('payment_id', 2)->where('sale_state_id', 2)->whereMonth('updated_at', '=', $month)->whereYear('updated_at', '=', $year)->count(),
            'top_3_sales' =>  DB::select('SELECT product.name, sum(qty) as qty FROM sale_detail left join product on product.id = sale_detail.product_id group by sale_detail.product_id, product.name order by sum(qty) desc limit 3'),
            'sales_3_last_month' =>  DB::select('SELECT SUM(total_price) as SUM, MONTH(now()) as MONTH FROM sale_detail where MONTH(now()) = MONTH(updated_at) UNION SELECT SUM(total_price) as SUM, MONTH(date_sub(now(), INTERVAL 1 MONTH)) as MONTH FROM sale_detail where MONTH(date_sub(now(), INTERVAL 1 MONTH)) = MONTH(updated_at) UNION SELECT SUM(total_price) as SUM, MONTH(date_sub(now(), INTERVAL 2 MONTH)) as MONTH FROM sale_detail where MONTH(date_sub(now(), INTERVAL 2 MONTH)) = MONTH(updated_at)')
        ];
    }
}
