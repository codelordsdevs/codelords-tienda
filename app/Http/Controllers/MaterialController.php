<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Validator;

class MaterialController extends Controller {
	public function __construct() {
		$this->middleware('auth:api');
	}

	public function get() {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		return ProductResource::collection(Product::where('product_id', request('id'))->where('product_type_id', 3)->get());
	}

	public function put(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		$rules = [
			'name' => 'required|max:100|unique:product,name',
			'price' => 'integer',
			'stock' => 'required|integer',
			'discount_qty' => 'required|integer',
			'product_id' => 'required|integer|exists:product,id'
		];
		$customMessages = [
			'required' => ':attribute campo requerido.',
			'max' => ':attribute supera cantidad maxima de caracteres.',
			'unique' => ':attribute ya existe.',
			'integer' => ':attribute debe ser numerico.'
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return $validator->messages();
		} else {
			$father_product = Product::find(request('product_id'));
			$product = new Product();
			try {
				$product->name = request('name');
				$product->price = request('price');
				$product->stock = request('stock');
				$product->product_id = request('product_id');
				$product->share_stock_discount_qty = request('discount_qty');
				$product->product_type_id = 3;
				$product->share_stock = 1;
				$product->save();
				return [
					'status' => 'true',
					'message' => 'Producto creado',
					'products' => ProductResource::collection(Product::where('product_id', request('product_id'))->where('product_type_id', 3)->get())
				];
			} catch (Exception $ex) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}

	public function patch(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		if (request('id') != null && request('id') > 0) {
			$product = Product::find(request('id'));
			if (($product[0] != null)) {
				$rules = [
					'name' => 'required|max:100|unique:product,name',
					'price' => 'required|integer',
					'discount_qty' => 'required|integer',
				];
				$customMessages = [
					'required' => ':attribute campo requerido.',
					'max' => ':attribute supera cantidad maxima de caracteres.',
					'unique' => ':attribute ya existe.',
					'integer' => ':attribute debe ser numerico.'
				];
				$validator = Validator::make($request->all(), $rules, $customMessages);
				if ($validator->fails()) {
					return $validator->messages();
				} else {
					try {
						if ((request('name') != null) && (strlen(request('name')) > 0)) {
							$product->name = request('name');
						}
						$product->price = request('price');
						$product->share_stock_discount_qty = request('discount_qty');
						$product->save();
						return [
							'status' => 'true',
							'message' => 'Producto actualizado',
							'products' => ProductResource::collection(Product::where('product_id', $product->product_id)->where('product_type_id', 2)->with('product_images')->get())
						];
					} catch (Exception $ex) {
						//poner esto en un log
						return [
							'status' => 'false',
							'message' => 'Error interno',
						];
					}
				}
			} else {
				return [
					'status' => 'false',
					'message' => 'Producto no encontrado',
				];
			}
		}
		return [
			'status' => 'false',
			'message' => 'Request sin ID',
		];
	}

	public function delete(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		if (request('id') != null && request('id') > 0) {
			$product = Product::find(request('id'));
			if (($product != null) && ($product->stock == 0)) {
				try {
					$product->delete();
					return [
						'status' => 'true',
						'message' => 'Producto eliminado',
					];
				} catch (Exception $ex) {
					//poner esto en un log
					return [
						'status' => 'false',
						'message' => 'Error interno',
					];
				}
			} else {
				return [
					'status' => 'false',
					'message' => 'Producto no se puede eliminar, tiene stock mayor a 0',
				];
			}
		}
		return [
			'status' => 'false',
			'message' => 'Request sin ID',
		];
	}

	public function options(Request $request) {
	}
}
