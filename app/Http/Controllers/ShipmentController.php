<?php

namespace App\Http\Controllers;

use App\Models\DispatchAdress;
use Illuminate\Http\Request;
use App\Http\Resources\ShipmentResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


class ShipmentController extends Controller {

    public function __construct() {
        $this->middleware('auth:api', ['except' => []]);
    }

    public function post(Request $request) {
        return ShipmentResource::collection(DispatchAdress::where('user_id', auth()->user()->id)->get());
    }

    public function postWorker(Request $request) {
        return ShipmentResource::collection(DispatchAdress::where('user_id', request('id'))->get());
    }


    public function put(Request $request) {
        $rules = [
            'city_id' => [
                'required',
                'integer',
            ],
            'name' => [
                'required',
                'string',
            ],
            'address' => [
                'required',
                'string',
            ]
        ];
        $customMessages = [
            'required' => ':attribute campo requerido.',
            'max' => ':attribute supera cantidad maxima de caracteres.',
            'unique' => ':attribute ya existe.',
            'integer' => ':attribute debe ser numerico.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            //If you don't have an id in the request, a new one is added
            try {
                $ShipmentExists = DispatchAdress::where('id', request('id'))->where('user_id', auth()->user()->id)->first();
                if ($ShipmentExists) {

                    $ShipmentExists->user_id = auth()->user()->id;
                    $ShipmentExists->postal_code = request('postal_code');
                    $ShipmentExists->name = request('name');
                    $ShipmentExists->address = request('address');
                    $ShipmentExists->city_id = request('city_id');
                    $ShipmentExists->save();
                } else {

                    $shipment = new DispatchAdress();
                    $shipment->user_id = auth()->user()->id;
                    $shipment->postal_code = request('postal_code');
                    $shipment->name = request('name');
                    $shipment->address = request('address');
                    $shipment->city_id = request('city_id');
                    $shipment->save();
                    return [
                        'status' => 'true',
                        'message' => 'Address added successfully',
                    ];
                }
            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Internal Error',
                ];
            }
        }
    }


    public function delete(Request $request) {
        if (request('id') != null && request('id') > 0) {
            $Shipment = DispatchAdress::where('id', request('id'))->get()->first();
            if (($Shipment != null)) {
                try {
                    $Shipment->delete();
                    return [
                        'status' => 'true',
                        'message' => 'Shipping address removed',
                    ];
                } catch (Exception $ex) {
                    //poner esto en un log
                    return [
                        'status' => 'false',
                        'message' => 'Internal Error',
                    ];
                }
            }
        }
        return [
            'status' => 'false',
            'message' => 'id field is required',
        ];
    }
}
