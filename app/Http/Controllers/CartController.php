<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Product;
use App\Http\Resources\CartResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


class CartController extends Controller {

    public function __construct() {
        $this->middleware('auth:api', ['except' => []]);
    }


    public function get() {
        return CartResource::collection(Cart::with('product.product_images')->where('user_id', auth()->user()->id)->get());
    }

    public function post(Request $request) {
        if (request('user_id') != null && request('user_id') > 0) {
            return CartResource::collection(Cart::with('product.product_images')->where('user_id', auth()->user()->id)->get());
        }
    }

    public function put(Request $request) {
        $rules = [
            'qty' => [
                'required',
                'integer',
            ],
            'product_id' => [
                'required',
                'integer',
                /*
                function ($attribute, $value, $fail) {
                    $this->stock($attribute, $value, $fail);
                },
                */
            ],
        ];
        $customMessages = [
            'required' => ':attribute campo requerido.',
            'max' => ':attribute supera cantidad maxima de caracteres.',
            'unique' => ':attribute ya existe.',
            'integer' => ':attribute debe ser numerico.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            //validar el stock de los productos antes de esto
            try {
                $cartExists = Cart::where('product_id', request('product_id'))->where('user_id', auth()->user()->id)->with('product')->first();
                if ($cartExists) {
                    if ($cartExists->product->stock >= ($cartExists->qty + intval(request('qty')))) :
                        $cartExists->qty = $cartExists->qty + intval(request('qty'));
                        $cartExists->save();
                        return [
                            'status' => 'true',
                            'message' => 'Producto actualizado en el carrito',
                        ];
                    else :
                        return [
                            'status' => 'false',
                            'message' => 'Producto no tiene stock suficiente',
                        ];

                    endif;
                } else {
                    $cart = new Cart();
                    $cart->product_id = request('product_id');
                    $cart->qty = request('qty');
                    $cart->user_id = auth()->user()->id;
                    $cart->save();
                    return [
                        'status' => 'true',
                        'message' => 'Producto agregado al carrito',
                    ];
                }
            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Error interno',
                ];
            }
        }
    }

    public function patch(Request $request) {
        $rules = [
            'id' => 'required|integer|exists:shopping_cart,id',
            'qty' => 'required|integer',
            'selected' => 'required|boolean'
        ];
        $customMessages = [
            'required' => ':attribute campo requerido.',
            'max' => ':attribute supera cantidad maxima de caracteres.',
            'unique' => ':attribute ya existe.',
            'integer' => ':attribute debe ser entero.',
            'exists' => ':attribute no es valido'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            try {
                $cart = Cart::where('id', request('id'))->with('product')->get();
                if ($cart[0]->user_id == auth()->user()->id) :
                    if ($cart[0]->product->stock >= intval(request('qty'))) :
                        $cart[0]->selected = request('selected');
                        $cart[0]->qty = request('qty');

                        if (request('qty') == 0) :
                            $cart[0]->delete();
                        else :
                            $cart[0]->save();
                        endif;
                        return [
                            'status' => 'true',
                            'message' => 'Producto actualizado en el carrito',
                        ];
                    else :
                        return [
                            'status' => 'false',
                            'message' => 'Producto no tiene stock suficiente',
                        ];
                    endif;
                else :
                    return [
                        'status' => 'false',
                        'message' => 'Carrito no pertenece al usuario',
                    ];
                endif;
            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Error interno',
                ];
            }
        }
    }

    public function delete(Request $request) {
        if (request('id') != null && request('id') > 0) {
            $cart = Cart::where('id', request('id'))->get();
            if (($cart[0] != null)) {
                try {
                    if ($cart[0]->user_id == auth()->user()->id) :
                        $cart[0]->delete();

                        return [
                            'status' => 'true',
                            'message' => 'Producto eliminado del carrito',
                        ];
                    else :
                        return [
                            'status' => 'false',
                            'message' => 'Carrito no pertenece al usuario',
                        ];
                    endif;
                } catch (Exception $ex) {
                    //poner esto en un log
                    return [
                        'status' => 'false',
                        'message' => 'Error interno',
                    ];
                }
            }
        }
        return [
            'status' => 'false',
            'message' => 'Request sin ID',
        ];
    }

    public function options(Request $request) {
        if (request('selected') != null && request('selected') == 1) :
            return CartResource::collection(Cart::with('product.product_images')->where('user_id', auth()->user()->id)->where('selected', true)->get());
        endif;
        //Inicio vaciar carrito
        if (request('emptyCart') != null && request('emptyCart') == 1) {
            $cart = Cart::where('user_id', auth()->user()->id)->get();
            if (($cart[0] != null)) {
                try {
                    for ($x = 0; $x < count($cart); $x++) {
                        if ($cart[$x]->user_id == auth()->user()->id) {
                            $cart[$x]->delete();
                        }
                    }
                    return [
                        'status' => 'true',
                        'message' => 'Carrito limpiado',
                    ];
                } catch (Exception $ex) {
                    //poner esto en un log
                    return [
                        'status' => 'false',
                        'message' => 'Error interno',
                    ];
                }
            }
            return [
                'status' => 'false',
                'message' => 'Request sin ID',
            ];
        }
        //Fin vaciar carrito

        //Inicio Obtener productos nuevos
        return [
            'status' => 'error',
            'message' => 'not match',
        ];
    }

    public function stock($attribute, $value, $fail) {
        $product = Product::where('id', $value)->first();
        if ($product) {
            $cart = Cart::where('product_id', $value)->sum('qty');
            $availableProducts = intval($product->stock) - intval($cart);
            $availableProducts = $availableProducts - intval(request('qty'));
            if ($availableProducts < 0) {
                $fail('No hay stock suficiente de este producto.');
            }
        } else {
            $fail('Producto no existe');
        }
    }
}
