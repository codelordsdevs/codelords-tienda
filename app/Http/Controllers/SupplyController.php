<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Supply;
use App\Http\Resources\SupplyResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


class SupplyController extends Controller {

    public function __construct() {
        $this->middleware('auth:api', ['except' => []]);
    }

    public function post(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador
        if (request('product_id') != null && request('product_id') > 0) {
            return SupplyResource::collection(Supply::with('product')->where('product_id',request('product_id'))->get());
        }
    }

    public function put(Request $request) {
        //inicio validacion perfil administrador
        if (auth()->user()->profile_id != 2) {
            return [
                'status' => 'false',
                'message' => 'Usuario no autorizado',
            ];
        }
        //fin validacion perfil administrador

        $rules = [
            'product_id' => 'required|integer|exists:product,id',
            'qty' => 'required|integer',
            'message' => 'required|string',
        ];
        $customMessages = [
            'required' => ':attribute campo requerido.',
            'max' => ':attribute supera cantidad maxima de caracteres.',
            'unique' => ':attribute ya existe.',
            'integer' => ':attribute debe ser numerico.'
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            return $validator->messages();
        } else {
            //validar el stock de los productos antes de esto
            try {
                $suppy = new Supply();
                $suppy->product_id = request('product_id');
                $suppy->message = request('message');
                $suppy->qty = request('qty');
                $suppy->save();

                $product = Product::find(request('product_id'));
                $product->stock = $product->stock + intval(request('qty'));
                $product->save();

                return [
                    'status' => 'true',
                    'message' => 'Stock cargado al producto',
                    'supplies' => SupplyResource::collection(Supply::with('product')->where('product_id',request('product_id'))->get()),
                ];
            } catch (Exception $ex) {
                //poner esto en un log
                return [
                    'status' => 'false',
                    'message' => 'Error interno',
                ];
            }
        }
    }
}
