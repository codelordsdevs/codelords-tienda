<?php

namespace App\Http\Controllers;

use App\Http\Resources\RegionResource;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class RegionController extends Controller
{

    public function post(Request $request){
        if(request('region_id') != null ){
            return RegionResource::collection(Region::where('id',request('region_id'))->get());
        }}

    public function get(Request $request){
        return RegionResource::collection(Region::whereHas('cities', function ($query) {
            return $query->where('active', 1);
        })->get());
    }
}
