<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller {
	public function __construct() {
		$this->middleware('auth:api', ['except' => ['get']]);
	}



	public function get() {
		return CategoryResource::collection(Category::where('category_id', null)->with('subcategories')->get());
	}

	public function put(Request $request) {
		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador


		$rules = [
			'category_name' => 'required|string|unique:category,name',
		];
		$customMessages = [
			'required' => ':attribute campo requerido.',
			'unique' => ':attribute ya existe.',
			'string' => ':attribute debe ser texto.',
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return $validator->messages();
		} else {
			try {
				$category = new Category();
				$category->name = request('category_name');
				$category->category_id = null;
				$category->save();

				return [
					'status' => 'true',
					'message' => 'Nueva Categoria insertada',
					'categories' => CategoryResource::collection(Category::where('category_id', null)->with('subcategories')->get()),
				];
			} catch (Exception $ex) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}

	public function patch(Request $request) {

		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador

		$rules = [
			'id' => 'required|integer|exists:category,id',
			'category_name' => 'required|string|unique:category,name',
		];
		$customMessages = [
			'required' => ':attribute campo requerido.',
			'exists' => ':attribute ya existe.',
			'integer' => ':attribute debe ser texto.',
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return $validator->messages();
		} else {
			try {
				$category = Category::find(request('id'));
				$category->name = request('category_name');
				$category->save();
				return [
					'status' => 'true',
					'message' => 'Categoria actualizada',
					'categories' => CategoryResource::collection(Category::where('category_id', null)->with('subcategories')->get()),
				];
			} catch (Exception $ex) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}


	public function delete(Request $request) {

		//inicio validacion perfil administrador
		if (auth()->user()->profile_id != 2) {
			return [
				'status' => 'false',
				'message' => 'Usuario no autorizado',
			];
		}
		//fin validacion perfil administrador


		$rules = [
			'id' => 'required|integer|exists:category,id',
		];
		$customMessages = [
			'required' => ':attribute campo requerido.',
			'exists' => ':attribute ya existe.',
			'integer' => ':attribute debe ser texto.',
		];
		$validator = Validator::make($request->all(), $rules, $customMessages);
		if ($validator->fails()) {
			return $validator->messages();
		} else {
			try {
				$category = Category::with(['products', 'subcategories'])->find(request('id'));
				if (count($category->products) == 0) {
					if (count($category->subcategories) == 0) {
						$category->delete();
						return [
							'status' => 'true',
							'message' => 'Categoria eliminada',
							'categories' => CategoryResource::collection(Category::where('category_id', null)->with('subcategories')->get()),
						];
					} else {
						return [
							'status' => 'false',
							'message' => 'No se puede eliminar la categoria por que existen subcategorias asociadas a ella.',
						];
					}
				} else {
					return [
						'status' => 'false',
						'message' => 'No se puede eliminar la categoria por que existen productos asociados a ella.'
					];
				}
			} catch (Exception $ex) {
				//poner esto en un log
				return [
					'status' => 'false',
					'message' => 'Error interno',
				];
			}
		}
	}
}
