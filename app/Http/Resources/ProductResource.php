<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ProductResource extends JsonResource {
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        $date1 = Carbon::now();
        $date2 = new Carbon($this->special_price_end_date);
        $diff = $date1->diff($date2);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'stock' => $this->stock,
            'special_price' => $this->special_price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'category_id' => $this->category_id,
            'featured' => $this->featured,
            'description' => $this->description,
            'special_price_end_date' => $this->special_price_end_date,
            'isOffer' => $diff,
            'product_images' => $this->product_images,
            'category' => $this->category,
            'formated_price' => number_format($this->price, 0, ".", "."),
            'formated_special_price' => number_format($this->special_price, 0, ".", "."),
            'color_code' => $this->color_code,
            'share_stock' => $this->share_stock,
            'product_type_id' => $this->product_type_id,
            'subproducts' => $this->subproducts,
            'share_stock_discount_qty' => $this->share_stock_discount_qty,
        ];
    }
}
