<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ConfigResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'primary_color' => $this->primary_color,
            'secondary_color' => $this->secondary_color,
            'url_logo' => $this->url_logo,
            'url_image_carousel1' => $this->url_image_carousel1,
            'url_image_carousel2' => $this->url_image_carousel2,
            'url_image_parallax' => $this->url_image_parallax,
            'url_image_products' => $this->url_image_products,
            'url_facebook' => $this->url_facebook,
            'url_instagram' => $this->url_instagram,
            'url_twitter' => $this->url_twitter,
            'url_linkedin' => $this->url_linkedin,
            'email_contact' => $this->email_contact,
            'phone_number' => $this->phone_number,
            'whatsapp_number' => $this->whatsapp_number,
            'address' => $this->address,
            'deposit_name' => $this->deposit_name,
            'deposit_rut' => $this->deposit_rut,
            'deposit_email' => $this->deposit_email,
            'deposit_number' => $this->deposit_number,
            'deposit_bank' => $this->deposit_bank,
            'deposit_account_type' => $this->deposit_account_type
        ];
    }
}
