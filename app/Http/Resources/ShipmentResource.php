<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShipmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'postal_code' => $this->postal_code,
            'name' => $this->name,
            'address' => $this->address,
            'city_id' => $this->city_id,
            'user_id' => $this->user_id,
            'receiver_name'  => $this->receiver_name,
            'phone'  => $this->phone,
            'email' => $this->email,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
