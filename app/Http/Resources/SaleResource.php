<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SaleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'payment_id' => $this->payment_id,
            'sale_state_id' => $this->sale_state_id,
            'transaction_code' => $this->transaction_code,
            'address' => $this->address,
            'city_id' => $this->city_id,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'receiver_name'  => $this->receiver_name,
            'phone'  => $this->phone,
            'email' => $this->email,
            'postal_code' => $this->postal_code,
            'pickup' => $this->pickup,
            'sale_detail' => $this->sale_detail,
            'sale_state' => $this->sale_state,
        ];
    }
}
