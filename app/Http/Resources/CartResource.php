<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Product;
use Carbon\Carbon;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product = Product::with('product_images')->find($this->product->id);
        $date1 = Carbon::now();
        $date2 = new Carbon($this->product->special_price_end_date);
        $diff = $date1->diff($date2);
        $product->isOffer = $diff;
        $product->formated_price = number_format($product->price, 0, ".", ".");
        $product->formated_special_price = number_format($product->special_price, 0, ".", ".");

        return [
            'id' => $this->id,
            'product' => $product,
            'qty' => $this->qty,
            'user' => $this->user,
            'selected' => $this->selected,
        ];
    }
}
