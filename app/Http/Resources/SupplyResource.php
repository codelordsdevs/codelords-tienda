<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Product;
use Carbon\Carbon;

class SupplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product = Product::find($this->product->id);
        return [
            'id' => $this->id,
            'product' => $product,
            'qty' => $this->qty,
            'message' => $this->message,
            'created_at' => $this->created_at,
        ];
    }
}
