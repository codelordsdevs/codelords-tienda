<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('transaction', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_code', 100)->nullable();
            $table->float('amount');
            $table->unsignedBigInteger('sale_id')->index();
            $table->unsignedBigInteger('payment_id')->index();
            $table->boolean('confirmed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('transaction');
    }
}
