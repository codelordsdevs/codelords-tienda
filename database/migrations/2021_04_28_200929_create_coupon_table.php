<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('coupon', function (Blueprint $table) {
            $table->id();
            $table->string('code', 100);
            $table->unsignedBigInteger('user_id')->index()->nullable();
            $table->unsignedBigInteger('product_id')->index()->nullable();
            $table->unsignedBigInteger('category_id')->index()->nullable();
            $table->integer('qty');
            $table->integer('discount');
            $table->boolean('is_percentage');
            $table->timestamp('expired_at')->nullable();
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('coupon');
    }
}
