<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration {
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'product';

    /**
     * Run the migrations.
     * @table product
     *
     * @return void
     */
    public function up() {
        Schema::create($this->tableName, function (Blueprint $table) {

            $table->id();
            $table->string('name', 100);
            $table->double('price', 15, 2);
            $table->integer('stock');
            $table->double('special_price', 15, 2)->nullable();
            $table->unsignedBigInteger('category_id')->index()->nullable();
            $table->unsignedBigInteger('product_id')->index()->nullable();
            $table->unsignedBigInteger('product_type_id')->index();
            $table->tinyInteger('featured')->nullable();
            $table->tinyInteger('share_stock');
            $table->longText('description')->nullable();
            $table->timestamp('special_price_end_date')->nullable();
            $table->softDeletes('deleted_at', 0);
            $table->string('color_code', 100)->nullable();
            $table->integer('share_stock_discount_qty')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->tableName);
    }
}
