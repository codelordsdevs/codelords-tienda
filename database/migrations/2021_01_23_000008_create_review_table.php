<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'review';

    /**
     * Run the migrations.
     * @table review
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            
            $table->id();
            $table->string('name', 100);
            $table->string('email', 100);
            $table->string('title', 50);
            $table->longText('description');
            $table->integer('rating');
            $table->unsignedBigInteger('product_id')->index();
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
