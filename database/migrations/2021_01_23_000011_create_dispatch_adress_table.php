<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispatchAdressTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'dispatch_adress';

    /**
     * Run the migrations.
     * @table dispatch_adress
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('postal_code')->nullable();
            $table->string('name', 100);
            $table->string('address', 100);
            $table->string('address_extrainfo', 100)->nullable();
            $table->string('receiver_name', 100)->nullable();
            $table->string('phone', 15)->nullable();
            $table->string('email', 100)->nullable();
            $table->unsignedBigInteger('city_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();

            


            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
