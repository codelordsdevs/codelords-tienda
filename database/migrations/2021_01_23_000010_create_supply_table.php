<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplyTable extends Migration {
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'supply';
    /**
     * Run the migrations.
     * @table supply
     *
     * @return void
     */
    public function up() {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->id();
            $table->integer('qty');
            $table->text('message');
            $table->unsignedBigInteger('product_id')->index();
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->tableName);
    }
}
