<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToUserModuleTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('user_module', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('module_id')->references('id')->on('module');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('user_module', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['module_id']);
        });
    }
}
