<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductImageTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'product_image';

    /**
     * Run the migrations.
     * @table product_image
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
  
            $table->id();
            $table->longText('url');
            $table->integer('order');
            $table->tinyInteger('featured');
            $table->unsignedBigInteger('product_id')->index();
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();

 



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
