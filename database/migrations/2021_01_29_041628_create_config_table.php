<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('config', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('primary_color', 10);
            $table->string('secondary_color', 10);
            $table->longText('url_logo');
            $table->longText('url_image_carousel1');
            $table->longText('url_image_carousel2');
            $table->longText('url_image_parallax');
            $table->longText('url_image_products');
            $table->string('url_facebook', 100)->nullable();
            $table->string('url_instagram', 100)->nullable();
            $table->string('url_twitter', 100)->nullable();
            $table->string('url_linkedin', 100)->nullable();
            $table->string('email_contact', 100)->nullable();
            $table->string('phone_number', 100)->nullable();
            $table->string('whatsapp_number', 100)->nullable();
            $table->string('address', 150)->nullable();
            $table->string('deposit_name', 150)->nullable();
            $table->string('deposit_rut', 20)->nullable();
            $table->string('deposit_email', 150)->nullable();
            $table->string('deposit_number', 150)->nullable();
            $table->string('deposit_bank', 150)->nullable();
            $table->string('deposit_account_type', 150)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('config');
    }
}
