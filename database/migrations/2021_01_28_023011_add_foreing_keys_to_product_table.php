<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingKeysToProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function (Blueprint $table) {         
		$table->foreign('category_id')->references('id')->on('category');
		$table->foreign('product_id')->references('id')->on('product');
		$table->foreign('product_type_id')->references('id')->on('product_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product', function (Blueprint $table) {
		$table->dropForeign(['product_id']);
		$table->dropForeign(['category_id']);
		$table->dropForeign(['product_type_id']);
        });
    }
}
