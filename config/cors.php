<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cross-Origin Resource Sharing (CORS) Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure your settings for cross-origin resource sharing
    | or "CORS". This determines what cross-origin operations may execute
    | in web browsers. You are free to adjust these settings as needed.
    |
    | To learn more: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
    |
    */

    'paths' => ['api/*'],

    'allowed_methods' => ['GET','POST','PUT', 'PATCH', 'OPTIONS', 'DELETE'],

    'allowed_origins' => [env('APP_FRONT_URL', 'http://127.0.0.1:8000'), env('APP_ADMIN_URL'), env('FLOW_API_URL')],

    'allowed_origins_patterns' => [],

    'allowed_headers' => ['Content-Type','Accept','Authorization'],

    'exposed_headers' => [],

    'max_age' => 0,

    'supports_credentials' => true,

];
