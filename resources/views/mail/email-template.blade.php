<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../assets/images/favicon/1.png" type="image/x-icon">
    <link rel="shortcut icon" href="../assets/images/favicon/1.png" type="image/x-icon">
    <title>Asesorar.cl</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <style type="text/css">
        body {
            text-align: center;
            margin: 0 auto;
            width: 650px;
            font-family: 'Open Sans', sans-serif;
            background-color: #e2e2e2;
            display: block;
        }

        ul {
            margin: 0;
            padding: 0;
        }

        li {
            display: inline-block;
            text-decoration: unset;
        }

        a {
            text-decoration: none;
        }

        h5 {
            margin: 10px;
            color: #777;
        }

        .text-center {
            text-align: center;
        }

        .text-justify {
            text-align: justify;
            margin-left: 50px;
            margin-right: 50px;
        }

        .main-bg-light {
            background-color: #fafafa;
        }

        .title {
            color: #444444;
            font-size: 22px;
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 0;
            padding-bottom: 0;
            text-transform: capitalize;
            display: inline-block;
            line-height: 1;
        }

        .menu {
            width: 100%;
        }

        .menu li a {
            text-transform: capitalize;
            color: #444;
            font-size: 16px;
            margin-right: 15px
        }

        .main-logo {
            width: 180px;
            padding: 10px 20px;
        }

        .product-box .product {
            /*border:1px solid #ddd;*/
            text-align: center;
            position: relative;
            margin: 0 15px;
        }

        .product-info {
            margin-top: 15px;
        }

        .product-info h6 {
            line-height: 1;
            margin-bottom: 0;
            padding-bottom: 5px;
            font-size: 14px;
            font-family: "Open Sans", sans-serif;
            color: #777;
            margin-top: 0;
        }

        .product-info h4 {
            font-size: 16px;
            color: #444;
            font-weight: 700;
            margin-bottom: 0;
            margin-top: 5px;
            padding-bottom: 5px;
            line-height: 1;
        }

        .add-with-banner>td {
            padding: 0 15px;
        }

        .footer-social-icon tr td img {
            margin-left: 5px;
            margin-right: 5px;
        }
    </style>
</head>

<body style="margin: 20px auto;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff; -webkit-box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);">
        <tbody>
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class="header">
                            <td align="left" valign="top">
                                <img src="{{ asset('image/logo_web.png') }}" class="main-logo">
                            </td>
                            <td class="menu" align="right">
                                <ul>
                                    <li style="display: inline-block;text-decoration: unset"><a href="{{ env('APP_FRONT_URL') }}" style="text-transform: capitalize;color:#444;font-size:16px;margin-right:15px;text-decoration: none;">Inicio</a>
                                    </li>
                                    <li style="display: inline-block;text-decoration: unset"><a href="{{ env('APP_FRONT_URL') }}/productlist" style="text-transform: capitalize;color:#444;font-size:16px;margin-right:15px;text-decoration: none;">Productos</a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td><img src="{{ asset('/images/email-temp/1.png') }}" alt="" style="width: 100%;"></td>
                        </tr>
                    </table>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <h2 class="text-center">¡Buenas tardes!</h2>
                                <p class="text-justify">
                                    Somos <b><a href="https://www.asesorar.cl">Asesorar.cl</a></b> una empresa creada por un grupo de asesores de <b>salud</b> y <b>bancos</b>, que buscamos poder entregarles a nuestros clientes la mejor asesoría de forma <b>gratuita</b> con el fin de poder sacar el mejor provecho a un plan de salud o a productos bancarios.
                                </p>
                                <h2 class="text-center">¿Cómo trabajamos?</h2>
                                <p class="text-justify">
                                    Dependiendo de tu interés si es sobre Isapre o Bancos, te puedes poner en contacto con nosotros respondiendo este mail o ingresando directamente a nuestra página <a href="https://www.asesorar.cl">www.asesorar.cl</a> en donde te contactara a la brevedad uno de nuestros asesores quien te guiara y orientara para escoger la mejor opción según tus necesidades.
                                </p>
                            </td>
                        </tr>
                    </table>
                    <table class="main-bg-light text-center" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-top:30px;">
                        <tr>
                            <td style="padding: 30px;">
                                <div>
                                    <h4 class="title" style="margin:0;text-align: center;">Siguenos</h4>
                                </div>
                                <table border="0" cellpadding="0" cellspacing="0" class="footer-social-icon" align="center" class="text-center" style="margin-top:20px;">
                                    <tr>
                                        <td>
                                            <a href="{{ env('URL_FACEBOOK') }}"><img src="{{ asset('/images/email-temp/facebook.png') }}" alt=""></a>
                                        </td>
                                        <td>
                                            <a href="{{ env('URL_TWITTER') }}"><img src="{{ asset('/images/email-temp/twitter.png') }}" alt=""></a>
                                        </td>
                                        <td>
                                            <a href="{{ env('URL_LINKEDIN') }}"><img src="{{ asset('/images/email-temp/linkedin.png') }}" alt=""></a>
                                        </td>
                                    </tr>
                                </table>
                                <div style="border-top: 1px solid #ddd; margin: 20px auto 0;"></div>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 20px auto 0;">
                                    <tr>
                                        <td>
                                            <p style="font-size:13px; margin:0;">2021 - 22 Copyright by Codelords.cl</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>