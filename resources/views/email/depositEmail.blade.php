<!DOCTYPE html>
<html>

<body>
    <h1>{{ $details['title'] }}</h1>
    <div>
        <b>Banco:</b>&nbsp;{{ $details['banc'] }}<br></br>
        <b>Tipo de cuenta:</b>&nbsp;{{ $details['accountType'] }}<br></br>
        <b>Numero de cuenta:</b>&nbsp;{{ $details['accountNumber'] }}<br></br>
        <b>Rut:</b>&nbsp;{{ $details['rut'] }}<br></br>
        <b>Nombre titular:</b>&nbsp;{{ $details['name'] }}<br></br>
    </div>
    <p>Gracias por comprar con nosotros</p>
</body>

</html>