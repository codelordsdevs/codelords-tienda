<?php

use Illuminate\Support\Facades\Route;
use App\Models\Sale;
use App\Models\Product;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/', function () {
  //return 'jeje';
  //$sale = Sale::with('sale_detail.product.product_images')->with('city.region')->find(1);
  //return view('mail.email-template2')->with('sale', $sale )->with('toMail', 'antonio@ticroom.cl');
  //   $email = new health_bank_campaign('1', 'antonio@ticroom.cl', '1');
  //   Mail::to('antonio@ticroom.cl')->send($email);
  //   return 'ok';
});

Route::get('/login-message', function(){
	return response()->json(['status' => '0', 'message' => 'No existe session']);
});

Route::get('/email/verify/{id}/{hash}', [App\Http\Controllers\UserController::class, 'verify'])->name('verification.verify');

Route::post('/payment_confirm', [App\Http\Controllers\PaymentController::class, 'confirmPaymentFlow'])->name('confirmPaymentFlow');
Route::get('/flow_redirect/{id}', [App\Http\Controllers\PaymentController::class, 'redirectPaymentFlow'])->name('redirectPaymentFlow');

