<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Http\Resources\UserResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */



//inicio rutas usuario
Route::group([
	'middleware' => 'api',
	'prefix' => 'auth'
], function ($router) {
	Route::post('login', 'App\Http\Controllers\AuthController@login');
	Route::post('logout', 'App\Http\Controllers\AuthController@logout');
	Route::post('refresh', 'App\Http\Controllers\AuthController@refresh');
	Route::post('me', 'App\Http\Controllers\AuthController@me');
});
//fin rutas usuario

//inicio rutas usuario
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::get('/worker', [App\Http\Controllers\UserController::class, 'getWorker']);
	Route::get('/users', [App\Http\Controllers\UserController::class, 'getUsers']);
	Route::put('/user', [App\Http\Controllers\UserController::class, 'put']);
	Route::post('/worker', [App\Http\Controllers\UserController::class, 'postWorker']);
	Route::post('/user', [App\Http\Controllers\UserController::class, 'postUserByEmail']);
	Route::put('/worker', [App\Http\Controllers\UserController::class, 'putWorker']);
	Route::patch('/user', [App\Http\Controllers\UserController::class, 'patch']);
	Route::patch('/userpassword', [App\Http\Controllers\UserController::class, 'changePassword']);
	Route::options('/user', [App\Http\Controllers\UserController::class, 'options']);
	Route::options('/worker', [App\Http\Controllers\UserController::class, 'optionsWorker']);
});
//fin rutas usuario

//inicio rutas producto
Route::group([
	'middleware' => 'api',
], function ($router) {

	Route::get('/product', [App\Http\Controllers\ProductController::class, 'get']);
	Route::post('/product', [App\Http\Controllers\ProductController::class, 'post']);
	Route::put('/product', [App\Http\Controllers\ProductController::class, 'put']);
	Route::patch('/product', [App\Http\Controllers\ProductController::class, 'patch']);
	Route::delete('/product', [App\Http\Controllers\ProductController::class, 'delete']);
	Route::options('/product', [App\Http\Controllers\ProductController::class, 'options']);
});
//fin rutas producto

//inicio rutas supply
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::post('/supply', [App\Http\Controllers\SupplyController::class, 'post']);
	Route::put('/supply', [App\Http\Controllers\SupplyController::class, 'put']);
});
//fin rutas supply

//inicio rutas tag
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::post('/tag', [App\Http\Controllers\TagController::class, 'post']);
	Route::put('/tag', [App\Http\Controllers\TagController::class, 'put']);
	Route::delete('/tag', [App\Http\Controllers\TagController::class, 'delete']);
});
//fin rutas tag

//inicio rutas coupon
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::get('/coupon', [App\Http\Controllers\CouponController::class, 'get']);
	Route::post('/coupon', [App\Http\Controllers\CouponController::class, 'post']);
	Route::put('/coupon', [App\Http\Controllers\CouponController::class, 'put']);
	Route::delete('/coupon', [App\Http\Controllers\CouponController::class, 'delete']);
});
//fin rutas coupon

//inicio rutas subproducto
Route::group([
	'middleware' => 'api',
], function ($router) {

	Route::get('/subproduct', [App\Http\Controllers\SubproductController::class, 'get']);
	Route::post('/subproduct', [App\Http\Controllers\SubproductController::class, 'post']);
	Route::put('/subproduct', [App\Http\Controllers\SubproductController::class, 'put']);
	Route::patch('/subproduct', [App\Http\Controllers\SubproductController::class, 'patch']);
	Route::delete('/subproduct', [App\Http\Controllers\SubproductController::class, 'delete']);
	Route::options('/subproduct', [App\Http\Controllers\SubproductController::class, 'options']);
});
//fin rutas subproducto

//inicio rutas colorproducto
Route::group([
	'middleware' => 'api',
], function ($router) {

	Route::post('/colorproduct', [App\Http\Controllers\ColorproductController::class, 'get']);
	Route::put('/colorproduct', [App\Http\Controllers\ColorproductController::class, 'put']);
	Route::patch('/colorproduct', [App\Http\Controllers\ColorproductController::class, 'patch']);
	Route::delete('/colorproduct', [App\Http\Controllers\ColorproductController::class, 'delete']);
	Route::options('/colorproduct', [App\Http\Controllers\ColorproductController::class, 'options']);
});
//fin rutas colorproducto

//inicio rutas materia prima
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::post('/material', [App\Http\Controllers\MaterialController::class, 'get']);
	Route::put('/material', [App\Http\Controllers\MaterialController::class, 'put']);
	Route::patch('/material', [App\Http\Controllers\MaterialController::class, 'patch']);
	Route::delete('/material', [App\Http\Controllers\MaterialController::class, 'delete']);
	Route::options('/material', [App\Http\Controllers\MaterialController::class, 'options']);
});
//fin rutas materia prima

//inicio rutas carrito
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::get('/cart', [App\Http\Controllers\CartController::class, 'get']);
	Route::post('/cart', [App\Http\Controllers\CartController::class, 'post']);
	Route::put('/cart', [App\Http\Controllers\CartController::class, 'put']);
	Route::patch('/cart', [App\Http\Controllers\CartController::class, 'patch']);
	Route::delete('/cart', [App\Http\Controllers\CartController::class, 'delete']);
	Route::options('/cart', [App\Http\Controllers\CartController::class, 'options']);
});
//fin rutas carrito

//Inicio rutas config
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::get('/config', [App\Http\Controllers\ConfigController::class, 'get']);
	Route::post('/config', [App\Http\Controllers\ConfigController::class, 'post']);
	Route::get('/login-message', function () {
		return response()->json(['status' => '0', 'message' => 'No existe session']);
	})->name('login');
});
//fin rutas config

//Inicio rutas shipment
Route::group([
	'middleware' => 'api',
], function ($request) {
	Route::post('/shipment', [App\Http\Controllers\ShipmentController::class, 'post']);
	Route::post('/shipmentWorker', [App\Http\Controllers\ShipmentController::class, 'postWorker']);
	Route::put('/shipment', [App\Http\Controllers\ShipmentController::class, 'put']);
	Route::delete('/shipment', [App\Http\Controllers\ShipmentController::class, 'delete']);
});
//fin rutas shipment

//Inicio rutas sale
Route::group([
	'middleware' => 'api',
], function ($request) {
	Route::get('/sale', [App\Http\Controllers\SaleController::class, 'get']);
	Route::post('/sale', [App\Http\Controllers\SaleController::class, 'post']);
	Route::put('/sale', [App\Http\Controllers\SaleController::class, 'put']);
	Route::put('/saleworker', [App\Http\Controllers\SaleController::class, 'putworker']);
	Route::options('/sale', [App\Http\Controllers\SaleController::class, 'options']);
});
//fin rutas sale

//Inicio rutas region
Route::group([
	'middleware' => 'api',
], function ($request) {
	Route::post('/region', [App\Http\Controllers\RegionController::class, 'post']);
	Route::get('/region', [App\Http\Controllers\RegionController::class, 'get']);
});
//fin rutas region

//Inicio rutas region
Route::group([
	'middleware' => 'api',
], function ($request) {
	Route::post('/city', [App\Http\Controllers\CityController::class, 'post']);
	Route::get('/city', [App\Http\Controllers\CityController::class, 'get']);
	Route::patch('/city', [App\Http\Controllers\CityController::class, 'patch']);
});
//fin rutas region

//Inicio rutas category
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::get('/category', [App\Http\Controllers\CategoryController::class, 'get']);
	Route::put('/category', [App\Http\Controllers\CategoryController::class, 'put']);
	Route::patch('/category', [App\Http\Controllers\CategoryController::class, 'patch']);
	Route::delete('/category', [App\Http\Controllers\CategoryController::class, 'delete']);
});
//fin rutas category

//Inicio rutas payment type
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::get('/payment', [App\Http\Controllers\PaymentController::class, 'getTypes']);
});
//fin rutas payment type


//Inicio rutas subcategory
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::get('/subcategory', [App\Http\Controllers\SubCategoryController::class, 'get']);
	Route::put('/subcategory', [App\Http\Controllers\SubCategoryController::class, 'put']);
	Route::patch('/subcategory', [App\Http\Controllers\SubCategoryController::class, 'patch']);
	Route::delete('/subcategory', [App\Http\Controllers\SubCategoryController::class, 'delete']);
});
//fin rutas subcategory

//Inicio rutas imagenes
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::post('/images', [App\Http\Controllers\ProductImageController::class, 'post']);
	Route::put('/images', [App\Http\Controllers\ProductImageController::class, 'put']);
	Route::patch('/images', [App\Http\Controllers\ProductImageController::class, 'patch']);
	Route::delete('/images', [App\Http\Controllers\ProductImageController::class, 'delete']);
});
//fin rutas imagenes


//Inicio rutas imagenes
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::post('/imageupload', [App\Http\Controllers\ProductImageController::class, 'put']);
});
//fin rutas imagenes

//Inicio rutas specialprice
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::post('/specialprice', [App\Http\Controllers\SpecialPriceController::class, 'post']);
	Route::get('/specialprice', [App\Http\Controllers\SpecialPriceController::class, 'get']);
	Route::delete('/specialprice', [App\Http\Controllers\SpecialPriceController::class, 'delete']);
});
//fin rutas specialprice

//Inicio rutas dashboard
Route::group([
	'middleware' => 'api',
], function ($router) {
	Route::get('dashboard', 'App\Http\Controllers\DashboardController@get');
});
//fin rutas dashboard
